import React, { useState, useEffect } from 'react'
import Navbar from '../../components/Navbar'
import { FaConciergeBell } from 'react-icons/fa'
import { AiFillPlusCircle, AiFillMinusCircle } from 'react-icons/ai'
import formatterMoney from '../../utils/formatMoney'
import { useHistory } from 'react-router-dom'
import Input from '../../components/Input'
import FETCH_API from '../../config/fetch_api'
import { onSave } from '../../config/config'
import { router } from '../../Constants.js'
import Loading from '../../components/Loading'
import Button from "../../components/Button"
import Gap from "../../components/Gap"
import InputPhone from '../../components/InputPhone'
import { useToasts } from 'react-toast-notifications'
import validateEmail from '../../utils/validateEmail';
import FormatPhone from "../../utils/FormatPhone"
import { encrypt, decrypt } from "../../utils/enkripsi"
import { formatDate, formatMinutes} from "../../utils/formatDate"
import uuid from 'react-uuid'
import api from '../../request'

export default function DetailOrder() {
    const { addToast } = useToasts()
    let history = useHistory();
    const [buttonOrder, setButtonOrder] = React.useState('delivery')
    const [dataOrder, setDataOrder] = React.useState([])
    const [total, setTotal] = React.useState({})
    const [loading, setLoading] = React.useState(true)
    const [loadingPembayaran, setLoadingPembayaran] = React.useState(false)
    const [detailPembayaran, setDetailPembayaran] = React.useState([])

    
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;
    var today = year + "-" + month + "-" + day;
    

    var d = new Date();
    let hours   = d.getHours() < 10 ? "0"+d.getHours() : d.getHours()
    let minutes = d.getMinutes() < 10 ? "0"+d.getMinutes() : d.getMinutes() 
    var n = hours +":"+minutes

    const [detailUsers, setDetailUsers] = React.useState({
        nama: '',
        email: '',
        nomerHp: '',
        date: today,
        time: n
    })

   function hitung(dataOrder) {
       const data = JSON.parse(localStorage.getItem('local_charges'))
        let xs = []
        let total = 0
        let qtyTotal = 0
        for(var i = 0; i < dataOrder.length; i++){
            let item = dataOrder[i]
            let subTotal = (item.item * item.usePrice)
            total += subTotal
            qtyTotal += item.item
            item.total = subTotal
        }
        return {
            item  : qtyTotal,
            total : total,
            grandTotal : total + (total * 10/100)
        }
   }



    async function onHitung(dataOrder){
        const data = JSON.parse(localStorage.getItem('local_charges'))
        let xs = []
        let total = 0
        let qtyTotal = 0
        let sc = 0
        let tax = 0

        for(var i = 0; i < dataOrder.length; i++){
            let item = dataOrder[i]
            let subTotal = (item.item * item.usePrice)
            total += subTotal
            qtyTotal += item.item
            item.total = subTotal
        }
        xs.push({
            label: 'Subtotal',
            value: total
        })
        data.forEach(d=>{
            if (d.pb1 == 0) {
                if ( (total > d.max_value) && (d.max_value > 0) ){
                    d.value = d.percent_value / 100 * d.max_value;
                }
                else {
                    d.value = (d.percent_value / 100 * total);
                }
                sc = d.value
                xs.push({
                    label: d.id + ' ' + d.percent_value + '%',
                    value: d.value
                })
            }
            if (d.pb1 == 1) {
                if ( (total > d.max_value) && ((d.max_value || 0) > 0) ){
                    d.value = d.max_value || 0;
                }
                else {
                    d.value = (d.percent_value / 100 * total);
                }
                tax = d.value
                xs.push({
                    label: d.id + ' ' + d.percent_value + '%',
                    value: d.value
                })
            }
        })


        xs.push({
            label: 'Total Pembayaran',
            value: total + sc + tax
        })
        setDetailPembayaran(xs)
        setDataOrder((old) =>[...old])
    }



    function onPressMinus(data){
        const pesanan = decrypt(JSON.parse(localStorage.getItem("pesanan")))
        const selectPesanan = pesanan.filter((items) => items['product_id'] == data['product_id'])
        data.item -= 1
        if(data.item  < 1){
            let dataLocal = decrypt(JSON.parse(localStorage.getItem('pesanan')))
            let getData   = dataLocal.filter(item => item['product_id'] !== data['product_id'])
            const deletPesanan = pesanan.filter((items) => items['product_id'] !== data['product_id'])
            localStorage.setItem("pesanan", JSON.stringify(encrypt(deletPesanan)))
            setDataOrder(getData)
            if (getData.length == 0) {
                history.push(`${router}/order`)
            }
            return
        }
        for(const xi of selectPesanan){
            if (xi['item'] > 0) {             
                xi['item'] -= 1
                xi['total'] -= data['price']
            }
        }
        localStorage.setItem("pesanan", JSON.stringify(encrypt(pesanan)))
        setDataOrder(pesanan)
        onHitung(pesanan)
    }   

    function onPressPlus(data){
        const pesanan = decrypt(JSON.parse(localStorage.getItem("pesanan")))
        const selectPesanan = pesanan.filter((items) => items['product_id'] == data['product_id'])
        data.item += 1
        for(const xi of selectPesanan){
            xi['item'] += 1
            xi['total'] += data['price']
        }
        localStorage.setItem("pesanan", JSON.stringify(encrypt(pesanan)))
        setDataOrder(pesanan)
        onHitung(pesanan)
    }

    async function refreshDataOrder() {
        let data = localStorage.getItem('pesanan')
        if (data == null) {
            setDataOrder([])
            data = []
        }
        else {
            data = decrypt(JSON.parse(data))
            console.log("pesanan", data)
            setDataOrder(data)
        }
        setLoading(false)
        onHitung(data)
    }


    useEffect(async() => {
        refreshDataOrder()
        const users = JSON.parse(localStorage.getItem('login'))
        const orderDate = JSON.parse(localStorage.getItem('orderDate'))
        if (users && !orderDate) {
            setDetailUsers(val => ({
                ...val,
                nama: users.nama,
                nomerHp: FormatPhone(users.telp),
                email: ''
            }))
        }
        if (orderDate) {
            setDetailUsers(val => ({
                ...val,
                nama: orderDate.nama,
                nomerHp: FormatPhone(orderDate.nomerHp),
                email: orderDate.email,
                date: orderDate.date,
                time: orderDate.time
            }))
        }

    },[])


    async function onOrder() {
        setLoadingPembayaran(true)
        // if (detailUsers['nomerHp'] == '' && !localStorage.getItem('login')) {
        if (false) {
            setLoadingPembayaran(false)
            addToast("No Telepon harus diisi", {
                appearance: 'error',
                autoDismiss: true,
            })
            return
        }
        else {
            let detailTrans = []
            const pesanan = decrypt(JSON.parse(localStorage.getItem("pesanan")))
            const branch = JSON.parse(localStorage.getItem('dataBranch'))
            const type = localStorage.getItem("type") 
            const coa = await api.apiPrepare.coaBranchAll({
                "userId": detailUsers.nama == "" ? "Admin" : detailUsers.nama,
            })

            const local_charges = JSON.parse(localStorage.getItem('local_charges'))


            let date = {
                date: formatDate(new Date),
                time: formatMinutes(new Date)
            }
            for (const items of pesanan){
                detailTrans.push(detailTrens(items, detailUsers, branch, coa.data[0], date))
            }

            let orderDate = {
                data: []
            }
            if (type == "tableQR") {            
                orderDate = await api.apiPrepare.get({ "id": branch.detailData })
            }
            let orderPrev = []
            for(const xi of orderDate.data){
                orderPrev.push({
                    item: xi.qty,
                    usePrice: xi.price,
                })
            }
            for(const xii of pesanan){
                orderPrev.push(xii)
            } 
            const count = Object.assign({}, detailPembayaran)           
            let data = []
            let hasil = 0
            if (orderDate.data !== 1) {              
                for(const temps of orderDate.data){
                    if (temps.product_id !== "QR") {        
                        data.push(temps)
                        hasil += (temps['price'] * temps['qty'])
                    }
                }
            }
            else {
                hasil = total.total
            }     

            let charges = local_charges.map(items => ({
                coa: items['coa'],
                id: items.id,
                max_value: items.max_value,
                min_value: items.min_value,
                pb1: items['pb1'],
                percent_value: items['percent_value'],
                value: items.id == "SC" ? count['1'].value : count['2'].value
            }))
            const dataDetailData = {
                "branch_id": branch.branchID,
                "grand_total_nett":  count['3'].value,
                "total":  count['0'].value,
                "sub_total":  count['0'].value,
                "grand_total": count['0'].value,
                "kembali": 0,
                "payment_cash_coa":"1.01.01.01.01",
                "payment_cash_coa_name":"KAS OMSET",
                "payment_cash_value": 0,
                "payment_type": 0,
                "payment_others_name": "",
                "payment_others_value":  count['0'].value,
                "payment_others_edc": "",
                "payment_others_coa": "",
                "dp_so": 0,
                "coa_dp_so": coa.data['0']['coa_dp_so'],
                "coa_dp_so_name": coa.data['0']['coa_dp_so_name']
            }



            if (type == 'takeaway') {
                dataDetailData.id = uuid()
                branch.detailData = dataDetailData.id
                dataDetailData.customer_id =  "URUZ"
                dataDetailData.customer_name =  "CUSTOMER URUZ"
                dataDetailData.marketing_id =  "admin"
                dataDetailData.marketing_name =  "admin"
                dataDetailData.default_warehouse_id = coa.data[0]['default_warehouse_id']
                dataDetailData.default_warehouse_name = coa.data[0]['default_warehouse_name']
                dataDetailData.note = ""
                dataDetailData.harga_a_note = ""
                dataDetailData.harga_a_note_show = ""
                dataDetailData.table_id = ""
                dataDetailData.jumlah_tamu = 2
                dataDetailData.discounts_value = 0
                dataDetailData.charges_value = 0
                dataDetailData.charges_value_pb1 = 0
                dataDetailData.charges_value_non_pb1 = 0
                dataDetailData.top = 0
                dataDetailData.payment_dc1_value = 0
                dataDetailData.payment_dc2_value = 0
                dataDetailData.payment_cc1_value = 0
                dataDetailData.payment_cc2_value = 0
                dataDetailData.so_id = ""
                dataDetailData.si_id = ""
                dataDetailData.created_by = ""
                dataDetailData.created_by_name = ""
                dataDetailData.created_date = ""
                dataDetailData.updated_by = ""
                dataDetailData.updated_by_name = ""
                dataDetailData.updated_date = ""
                dataDetailData.processed_by = ""
                dataDetailData.processed_by_name = ""
                dataDetailData.processed_date = ""
                dataDetailData.terminated_by = ""
                dataDetailData.terminated_by_name = ""
                dataDetailData.terminated_note = ""
                dataDetailData.terminated_date = ""
                dataDetailData.void_by = ""
                dataDetailData.void_by_name = ""
                dataDetailData.void_note = ""
                dataDetailData.void_date = ""
                dataDetailData.void_date = ""
                dataDetailData.is_accepted_rejected = ""
                dataDetailData.is_accepted_rejected_show = ""
                dataDetailData.accepted_rejected_by = ""
                dataDetailData.accepted_rejected_by_name = ""
                dataDetailData.accepted_rejected_note = ""
                dataDetailData.is_prepared = 0
                dataDetailData.is_prepared_show = ""
                dataDetailData.prepared_by = ""
                dataDetailData.prepared_by_name = ""
                dataDetailData.prepared_note = ""
                dataDetailData.is_billed = 0
                dataDetailData.is_billed_show = ""
                dataDetailData.billed_date = ""
                dataDetailData.billed_by = ""
                dataDetailData.billed_by_name = ""
                dataDetailData.picpath = ""
                dataDetailData.print_x_time = 0
                dataDetailData.is_checked = 0
                dataDetailData.is_checked_show = ""
                dataDetailData.checked_by = ""
                dataDetailData.checked_by_name = ""
                dataDetailData.checked_note = ""
                dataDetailData.posted_inv_status = 0
                dataDetailData.posted_inv_note = ""
                dataDetailData.posted_inv_date = ""
                dataDetailData.posted_acc_status = 0
                dataDetailData.posted_acc_note = ""
                dataDetailData.posted_acc_date = ""
                dataDetailData.cashlez_link = ""
                dataDetailData.cashlez_qr = ""
                dataDetailData.estimation_date = formatDate(new Date()) + ' ' + formatMinutes(new Date())
                dataDetailData.trans_date = formatDate(new Date()) + ' ' + formatMinutes(new Date())
            }
            if (type == "tableQR") {
                dataDetailData.id = branch.detailData
            }

            localStorage.setItem("detailData", JSON.stringify(encrypt(dataDetailData)))
            localStorage.setItem("dataBranch", JSON.stringify(branch))
            localStorage.setItem("detailTrans", JSON.stringify(encrypt(detailTrans)))
            localStorage.setItem("chargesNote", JSON.stringify(encrypt(charges)))



            const result = await onSave('save')
            console.log("sukses", result)
            if (result.status == "OK") {
                // detailUsers['nomerHp'] = '+62' + detailUsers['nomerHp']
                localStorage.setItem('orderDate', JSON.stringify(detailUsers))
                history.push(`${router}/transaksi-proses`)
                localStorage.removeItem('pesanan')
            }
            else {
                addToast("Print tidak valid", {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
            setLoadingPembayaran(false)
        }
    }

    return (
        <div>
            <Navbar urlBack="/order" type="back" label="Konfirmasi Pesanan"/>
            <div style={{ backgroundColor: '#F0F0F0' }}>
                <div style={{ padding: '25% 0 20px 0', display: 'flex', margin: '0 10px 0 10px', }}>
                    <FaConciergeBell color="#5451D6" size={25} />
                    <div style={{ marginLeft: 20}}>
                        <div className="fonts700" style={{ fontSize: 15}}>{JSON.parse(localStorage.getItem('dataBranch')).branchName}</div>
                        <div className="fonts400" style={{ fontSize: 12, color: '#7E7E7E'}}>{JSON.parse(localStorage.getItem('dataBranch')).branchAddress}</div>
                    </div>
                </div>
            </div>
            <div style={{ margin: '20px 10px 0 10px' }}>
                {
                    !JSON.parse(localStorage.getItem('login')) &&
                    <div style={{ display: 'flex', justifyContent: 'space-between', margin: '10px'}}>
                        <Button color="#fff" backgroundColor="#5451D6" onClick={() => { history.push(`${router}/login`)}}>Masuk</Button>
                        <Gap width={20} />
                        <Button color="#fff" backgroundColor="#5451D6" onClick={() => { history.push(`${router}/register`)}}>Daftar</Button>
                    </div>
                }
                {
                    !localStorage.getItem("login") &&
                    <div style={{ margin: '20px 10px', borderRadius: 5 }}>
                        {/* <Input onBlur={() => {
                            localStorage.setItem('orderDate', JSON.stringify(detailUsers))
                        }} value={detailUsers['nama']} 
                        onChange={(val) => setDetailUsers((old) => ({
                            ...old,
                            nama: val.target.value
                        }))} 
                        placeholder="Nama" label="Nama" />
                        <Gap height={10} /> */}
                        {/* <Input onBlur={() => {
                            if (validateEmail(detailUsers)) {
                                localStorage.setItem('orderDate', JSON.stringify(detailUsers))
                            }
                            else {
                                addToast("Email salah", {
                                    appearance: 'error',
                                    autoDismiss: true,
                                })
                                setDetailUsers((old) => ({
                                    ...old,
                                    email: ""
                                }))
                            }
                        }} value={detailUsers['email']} 
                        onChange={(val) => setDetailUsers((old) => ({
                            ...old,
                            email: val.target.value
                        }))} 
                        placeholder="example@mail.com" label="Email" />
                        <Gap height={10} /> */}
                        <InputPhone onBlur={() => {
                            localStorage.setItem('orderDate', JSON.stringify(detailUsers))
                        }} value={detailUsers['nomerHp']} onChange={(val) => setDetailUsers((old) => ({
                            ...old,
                            nomerHp: val
                        }))} placeholder="8123456789" label="Nomor Handphone" />
                    </div>
                }
                <div style={{ display: 'flex', justifyContent: 'space-between'}}>
                    <div className="fonts500" >Pesanan</div>
                    <div onClick={() => history.push(`${router}/order`)} className="fonts500" style={{ color: '#5451D6'}}>Tambah Pesanan</div>
                </div>
                <div style={{ margin: '20px 10px 0 10px', backgroundColor: '#fff', borderRadius: 5 }}>
                    {
                       dataOrder && dataOrder.map((items, index) => {
                            return (
                                <div key={index} style={{ display: 'flex', justifyContent: 'space-between', padding: '10px 0 10px 0' , margin: '0px 10px 0px 10px', borderBottom: '1px solid #BFBFBF' }}>
                                    <div>
                                        <div className="fonts700">{items['item']}x {items['product_name']}</div>
                                        <div className="fonts500" style={{ fontSize: 14, color: '#BFBFBF'}}>{items['note']}</div>
                                        <div style={{marginTop:"10%", display: 'flex', alignItems: 'center'}}>
                                            <AiFillMinusCircle onClick={()=>onPressMinus(items)} color="#5451D6" size={20}/>
                                            <span style={{paddingRight:'10px', paddingLeft:'10px', fontWeight: '700'}}>{items['item']}</span>
                                            <AiFillPlusCircle onClick={()=>onPressPlus(items)} color="#5451D6" size={20}/>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="fonts700">{formatterMoney(items['total'])}</div>
                                        <div onClick={() => {
                                            let number = 0
                                            let total = 0
                                            let dataOrder = localStorage.getItem('pesanan')
                                            let x = decrypt(JSON.parse(dataOrder))
                                            const result = x.filter(item => item['product_id'] !== items['product_id'])
                                            for(const xii of result){
                                                number += xii['item']
                                                total += xii['total']
                                            }
                                            localStorage.setItem('pesanan', JSON.stringify( encrypt(result)))
                                            refreshDataOrder()
                                        }} style={{ paddingTop: 10, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    {
                        loading &&
                        <Loading/>
                    }
                </div>
            </div>


            <div style={{ margin: '20px 10px 20px 10px' }}>
                <div className="fonts500" >Detail Pembayaran</div>
                <div style={{ margin: '20px 10px 0 10px', backgroundColor: '#fff', borderRadius: 5 }}>
                    {
                        detailPembayaran.map((items, index) => (
                            <div 
                                key={index} 
                                style={{ 
                                    display: 'flex', 
                                    justifyContent: 'space-between', 
                                    padding: '5px 0 5px 0', 
                                    margin: '0px 10px 0px 10px', 
                                    borderTop: items.label == 'Total Pembayaran' ? '1px solid #BFBFBF' : '0px' 
                                }}>
                                <div className={ items.label == 'Total Pembayaran' ? 'fonts700' : 'fonts400'}>{items.label}</div>
                                <div className="fonts400" style={{textAlign:'right'}}>{formatterMoney(items.value)}</div>
                            </div>
                        ))
                    }
                </div>
                <div style={{  margin: '20px 10px 0 10px' }}>
                    <Button 
                    loading={loadingPembayaran}
                    onClick={onOrder} color="#fff" backgroundColor="#5451D6" >
                        Order
                    </Button>
                </div>
            </div>
        </div>
    )
}

function detailTrens(items, orderDate, branch, detailData, date) {
    return {
        "product_id": items['product_id'],
        "warehouse_id": detailData['default_warehouse_id'],
        "coa_inv":  branch.coa_inv,
        "coa_sales": branch.coa_sales_inv,
        "name": items['product_name'],
        "qty": items['item'],
        "unit_id": "CUP",
        "qty_unit": 1,
        "unit_id_small": "PCS",
        "curr": "IDR",
        "rate": 1,
        "price": items['price'],
        "detail_disc1": 0,
        "detail_disc2": 0,
        "detail_disc_value": 0,
        "detail_note": items['note'],
        "total": items['total'],
        "indent": 0,
        "picpath1_thumb": items['picpath1_thumb'],
        "ordered_by": orderDate['nama'] == "" ? "Admin" : orderDate['nama'],
        "ordered_at": date.date + ' ' + date.time,
        "accepted_rejected_by": "",
        "accepted_rejected_note": "Chrome",
        "accepted_rejected_at": null,
        "prepared_by": "",
        "prepared_at": "",
        "kitchen_type_id": "",
        "promo_name": items['product_name'],
        "promo_qty":  items['item'],
        "promo_price": items['price'],
        "promo_paket": 0,
        "dtp_bkp": 1
    }
}