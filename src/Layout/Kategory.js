import React from "react";
import { useSelector, useDispatch } from "react-redux"
import { SetKategori, produkActions } from "../redux/actions/prepareActions"


export default function Kategory() {
    const dispatch = useDispatch()
    const state = useSelector(state => state?.dataPrepare)
    function onClickCategory(items) {
        let newArr = [...state?.kategory]
        for(const x of newArr){
            if (x.id == items.id) {
                x['selection'] = true
            }
            else {
                x['selection'] = false
            }
        }
        dispatch(SetKategori(newArr))
        dispatch({
            type: 'SET_GET_DATA_PRODUK',
            value: {
                data: [],
                loading: true
            }
        })
        dispatch(produkActions({
            category_id: items.id
        }))
    }
    
    return (
        <div className='scrollbar' style={{ margin: '20px 20px 0 20px', display: 'flex', alignItems: 'center', flexDirection: 'row', overflow: 'auto', whiteSpace: 'nowrap' }}>
            {
                state?.kategory?.map((items, index) => (
                    <div onClick={() => onClickCategory(items)} key={index} style={{ backgroundColor: items['selection'] ? "#5451D6" : '#fff', marginRight: state?.kategory?.length == index ? 0: 10, padding: '5px 20px', borderRadius: 20 }}>
                        <div className='fonts500' style={{ color: items['selection']? '#fff' : '#5451D6', fontSize: 12}}>{items.label}</div>
                    </div>
                ))
            }
        </div>
    )
}