import React from "react";
import { Link, useHistory } from 'react-router-dom'
import { router } from "../Constants"
import { BiFoodMenu } from "react-icons/bi"
import { FaConciergeBell } from "react-icons/fa"
import { RiFileList2Fill, RiFileList2Line } from "react-icons/ri"
import { useSelector } from "react-redux"

const dataTab = [
    {
        label: "Menu",
        icons:  <BiFoodMenu size={20} color="#808080"/>,
        icons_active: <BiFoodMenu  size={20} color="#5451D6"/>,
        url: `${router}/home`
    },
    {
        label: "Panggil Pelayan",
        icons:  <FaConciergeBell size={20} color="#808080"/>,
        icons_active: <FaConciergeBell  size={20} color="#5451D6"/>,
        url: `${router}/home/call-waiters`
    },
    {
        label: "Transaksi",
        icons:  <RiFileList2Line size={20} color="#808080"/>,
        icons_active: <RiFileList2Fill  size={20} color="#5451D6"/>,
        url: `${router}/home/lacak-pesanan`
    },
]


export default function TabBottom({ children }) {
    let history = useHistory()
    const state = useSelector(state => state?.dataPrepare?.order) 
    const order = state?.data?.[0]?.data?.length
    const [url, setUrl] = React.useState()

    React.useEffect(() => {
        history.listen( location =>  {
            setUrl(location?.pathname)
       });
       if (!url) {
           setUrl(window.location.pathname)
       }
    },[])

    return (
        <div>
            {children}
            <div className="jumlah-order">
                <div style={{ margin: 10, width: '100%', borderRadius: 5, display: 'flex', justifyContent: 'space-around', border: '1px solid #5451D6'}}>
                    {
                        dataTab?.map((items, index) => (
                            <div onClick={() => {
                                const dataBranch = JSON.parse(localStorage.getItem('dataBranch'))
                                if (dataBranch) {
                                    history.push(items.url)
                                }
                            }} key={index} style={{ textDecoration: 'none', flex: 1, alignItems: 'center', justifyContent: 'center', display: 'flex', flexDirection: 'column', position: 'relative'}}>
                                {
                                    url !== items.url ? (<>
                                        {items.icons}
                                    </>
                                    )
                                    : (
                                    <>
                                        {items.icons_active}
                                    </>
                                    )
                                }
                                <div style={{ fontSize: 10, color: url !== items.url ? '#808080' : '#5451D6'}}>{items.label}</div>
                                {
                                    items.label == "Transaksi" && <>
                                        {
                                            state.data.length !== 0 &&
                                            <div style={{ width: '20%', height: '100%', position: 'absolute', display: 'flex', justifyContent: 'flex-end'}}>
                                                <div style={{ height: 17, width: 17, backgroundColor: 'red', borderRadius: 19, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                                    <span style={{ color: '#fff', fontSize: order < 10 ? 10 : 9, fontWeight: 'bold' }}>{ order < 99 ? order : '+99'}</span>
                                                </div>
                                            </div>
                                        }
                                    </>
                                }
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}