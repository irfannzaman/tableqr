import React, { Component } from 'react'
import { BrowserRouter, Switch, Route, Redirect, Link } from 'react-router-dom'
import Order from '../Pages/Order/Order'
import Login from '../Pages/Auth/Login'
import KodeOTP from '../Pages/Auth/Otp'
import KodeOTPRegister from '../Pages/Auth/OtpRegister'
import Register from '../Pages/Auth/Register'
import detailorder from '../Pages/DetailOrder/detailorder'
import SearchAlamat from '../Pages/SearchAlamat/SearchAlamat'
import MetodePembayaran from '../Pages/MetodePembayaran/MetodePembayaran'
import LacakPesanan from '../Pages/LacakPesanan/LacakPesanan'
import ListLacakPesanan from '../Pages/LacakPesanan/ListLacakPesanan'
import editProfile from '../Pages/Auth/editProfile'
import Barcode from '../Pages/QRIS/Barcode'
import Bill from '../Pages/Bill/Bill'
import PaymentSucces from '../Pages/PaymentSucces/paymentsucces'
import TransactionProcessed from '../Pages/TransactionProcessed/TransactionProcessed'
import { CallWaiters } from '../Pages/CallWaiters'


import { TabBottom } from "../Layout"
import { setKeranjang, setOrder } from "../redux/actions/prepareActions"
import { get_CoaXendit } from "../redux/actions/xenditActions"
import { useDispatch } from "react-redux"
import api from '../request'
import { decrypt } from "../utils/enkripsi"

function TabBottoms(params) {
    const { match } = params

    return ( 
        <TabBottom>
            <Route exact path={match.url} component={Order}/>
            <Route path={match.url  + '/lacak-pesanan'} component={LacakPesanan}/>
            <Route path={match.url  +'/call-waiters'} component={CallWaiters}/>
        </TabBottom>
    )
}


function Apps() {
    const dispatch = useDispatch()

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,   
        function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    async function name(params) {
        const s = getUrlVars()
        const branchQueryString = decrypt(s.params)
        let users = {}
        if (branchQueryString) {
            localStorage.setItem("type", branchQueryString.type)
            localStorage.setItem("userServer", JSON.stringify(branchQueryString.clientID))  
            users.clientID = branchQueryString.clientID
            users.branch_id = branchQueryString.branchID
            const res = await api.apiPrepare.getPos({
                "branches"   : [branchQueryString.branchID]
            }) 
            let items = res.data[0]
            let getCurrentLoginData = {}
            getCurrentLoginData.branchID = items.id
            if (branchQueryString.detailDataID) {
                getCurrentLoginData.detailData = branchQueryString.detailDataID
            }
            getCurrentLoginData.branchName = items.name
            getCurrentLoginData.branchAddress = items.address
            getCurrentLoginData.branchTel = items.mobile
            getCurrentLoginData.picpath = items.picpath
            localStorage.setItem('dataBranch', JSON.stringify(Object.assign(getCurrentLoginData, items)))
            const local_charges = await api.apiPrepare.getCharge()
            localStorage.setItem("local_charges", JSON.stringify(local_charges.data))
            // setDataBranch(Object.assign(getCurrentLoginData, items))

            if (branchQueryString.type == "tableQR") {
                users.detailData = branchQueryString.detailDataID
            }
            // localStorage.setItem("userServer", JSON.stringify(branchQueryString.clientID))    
        }
        dispatch(setKeranjang())
        dispatch(setOrder())
        dispatch(get_CoaXendit())
    }

    React.useEffect(() => {
        name()
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    },[])



    return (
        <BrowserRouter>
            <Switch>
                <Route path='/tableqr/home' component={TabBottoms}/>
                {/* <Route path='/tableqr/home' component={TabBottom}/> */}
                {/* <Route path='/tableqr/order' component={Order}/> */}
                <Route path='/tableqr/lacak-pesanan' component={LacakPesanan}/>
                <Route path='/tableqr/login' component={Login}/>
                <Route path='/tableqr/kode-otp' component={KodeOTP}/>
                <Route path='/tableqr/kode-otp-register' component={KodeOTPRegister}/>
                <Route path='/tableqr/register' component={Register}/>
                <Route path='/tableqr/edit-profile' component={editProfile}/>
                <Route path='/tableqr/detail-order' component={detailorder}/>
                <Route path='/tableqr/search-alamat' component={SearchAlamat}/>
                <Route path='/tableqr/metode-pembayaran/:id' component={MetodePembayaran}/>
                <Route path='/tableqr/list-lacak-pesanan' component={ListLacakPesanan}/>
                <Route path='/tableqr/metode-pembayaran-qris/:id' component={Barcode}/>
                <Route path='/tableqr/transaksi-proses' component={TransactionProcessed}/>
                <Route path='/tableqr/bill' component={Bill}/>
                <Route path='/tableqr/payment-succes' component={PaymentSucces}/>
                <Redirect from="/"  to="/tableqr/home"/>
            </Switch>
        </BrowserRouter>
    )
}

export default Apps