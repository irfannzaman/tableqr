import FETCH_API from "./fetch_api";
import {formatDate} from "../utils/formatDate";
import { encrypt, decrypt } from "../utils/enkripsi"


export async function send_email(params) {
    console.log("params email", params)
    let users = {}
    if (localStorage.getItem('login')) {
        users.nama = JSON.parse(localStorage.getItem('login')).nama
    }
    else {
        users.nama = JSON.parse(localStorage.getItem('orderDate')).nama
        users.email = JSON.parse(localStorage.getItem('orderDate')).email
    }

    try {
        if (users.email !== "") {            
            const result = await FETCH_API('send_email_web/afterPayment', {
                "subjectnya": params.subject,
                "namaPembeli": users.nama,
                "carabayar"  : params.carabayar,
                "idTransaksi" : params.idTransaksi,
                "tglTransaksi": formatDate(new Date),
                "subTotal" : params.subTotal,
                "grandTotal" : params.grandTotal,
                "linkLihatDetail" : 'https://cool.uruz.id/tableqr/lacak-pesanan',
                "email" : users.email,
                "detailData" : params.detailData
            }, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImlyZmFuIiwiaWF0IjoxNjI1MjA0NTQ2LCJleHAiOjE2MjUzODA5NDZ9.pd_86GRYqBZzAhccYtXkydh-dlXesj2B6Ec5HexO4f4')
            console.log("email", result)
        }
    } catch (error) {
        console.log(error)
    }
}

export async function proses_data() {
    const userServer = JSON.parse(localStorage.getItem("userServer")) 
    const id = JSON.parse(localStorage.getItem("dataBranch"))

    let users = {}
    if (localStorage.getItem('login')) {
        users.nama = JSON.parse(localStorage.getItem('login')).nama
    }
    else {
        users.nama = JSON.parse(localStorage.getItem('orderDate')).nama
    }
    const process = await FETCH_API('process-web', {
        "from_origin": "*",
        "userServer" : userServer,
        "transCode": "si",
        "transId": id.detailData,
        "userId": "-"
    })
    
    return process
}


export async function onSave(state){
    const userServer = JSON.parse(localStorage.getItem("userServer")) 
    let dataDetailData = decrypt(JSON.parse(localStorage.getItem('detailData')))
    let detailTrans = decrypt(JSON.parse(localStorage.getItem('detailTrans')))
    dataDetailData['customer_id'] = "URUZ-TABLE-QR"
    let chargesNote = decrypt(JSON.parse(localStorage.getItem('chargesNote')))

    console.log("chargesNote", chargesNote)
    let type = localStorage.getItem('type')
    let url = 'trans-s-si-web/update'
    let transMode = "edit"
    
    if (type == "takeaway") {
        url = "trans-s-si-web/save"
        if (state == "save") {
            transMode = "new"
        }
        if (state == "proses") {
            transMode = "edit"
        }
    }
    if (type == "tableQR") {
        url = 'trans-s-si-web/update'
        // if (state == "save") {
        //     detailTrans = detailTrans
        // }
        if (state == "proses") {
            detailTrans = []
        }
    }

    let body = {
        "from_origin": "*",
        "userServer": userServer,
        "transMode": transMode,
        "userId": "-",
        "detailData": JSON.stringify(dataDetailData),
        "detailTrans": JSON.stringify(detailTrans),
        "discountsNote" : "[]",
        "chargesNote" : JSON.stringify(chargesNote ? chargesNote : [])
    }
    console.log("body", body)
    const result = await FETCH_API(url, body)
    return result
}