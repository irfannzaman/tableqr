import { SET_GET_DATA_PRODUK, SET_SHIFT, SET_KATEGORY, SET_KERANJANG, SET_LIST_KATEGORI, SET_ORDER } from "../constants/prepareConstants"
import api from "../../request"
import { decrypt } from "../../utils/enkripsi"
import {refreshCharges, onHitung} from "../../utils/onHitung"


export const getKategory = () => async dispatch => {
    try {
        const result = await api.apiPrepare.kategory({
            "from_origin": "*",
            "search1": "",
        })
        const data = result?.data
        
        const kat = data.map(item => ({
            label: item.id,
            id: item.id
        }))


        let x = []

        for(const i of kat){
            if (i.id !== "ADD ON") {
                x.unshift(i)
            }
        }
        x.unshift({
            label: "All Kategory",
            id: "all"
        })
        x[0]['selection'] = true
        dispatch({type: SET_KATEGORY, value: x});
        dispatch({type: SET_LIST_KATEGORI, value: x});
    } catch (error) {
        console.log(error)
    }
}
export const setOrder = (value) => async dispatch => {
    const orderDate = JSON.parse(localStorage.getItem('orderDate'))
    const dataBranch = JSON.parse(localStorage.getItem('dataBranch'))
    const login = JSON.parse(localStorage.getItem('login'))
    let tex = {}
    let local_charges = refreshCharges()
    for(const item of local_charges){
        tex['label'] = item.id
        tex['percent_value'] = item.percent_value
    }

    try {       
        let tlp = ""
        if (value) {
            tlp = value
        }
        else {            
            if (login) {
                tlp = login.telp 
            }
            else if (orderDate) {    
                tlp =     orderDate.nomerHp
            }
        }
        const res = await api.apiPrepare.getOrder({
            "noTelp": "",
            "id": dataBranch?.detailData
        })    

        console.log("order1", res)

        let getStatus = res.data.filter(x => x['product_id'] !== "QR")
        getStatus.sort((a, b) => new Date(b.trans_date) - new Date(a.trans_date))
        const s = onHitung(getStatus)

        let array = []
        let si_id = ''
        let total = []

        total.push({
            label: 'Subtotal',
            value: s.data.sub_total
        })

        for(const xi of s.chargeNotes){
            total.push({
                label: xi.id + " " + xi.percent_value + "%",
                value: xi.value
            })
        }

        total.push({
            label: 'Total',
            value: s.data.grand_total_net
        })

        for(const item of getStatus){
            let data = []
            for(const temps of getStatus){
                if (item.si_id == temps.si_id) {
                    data.push(temps)
                }
            }
            if (si_id !== item.si_id) { 
                si_id = item.si_id              
                array.push({
                    id: item.si_id,
                    si_id: item.si_id,
                    date: item.trans_date,
                    data: data,
                    trans_status: item.trans_status,
                    is_prepared: item.is_prepared,
                    loading: false,
                    is_accepted_rejected: item.is_accepted_rejected,
                    // subTotal: s.data.sub_total,
                    chagerNote: total,

                })
            }            
        }

        dispatch({type: SET_ORDER, value: {
            data: array,
            dataOrder: getStatus,
            loading: false
        }})
        
    } catch (error) {
        console.log(error)
    }
}
export const produkActions = (value = {}) => async dispatch => {
    const { currentPage = 0, category_id = "all", data = [], total_baris} = value
    let baris = 0
    try {
        const result = await api.apiPrepare.produk({
            "search1": "",
            "pageSize" : "10",
            "currentPage" : currentPage.toString(),
            "category_id" : category_id
        })


        console.log("sukses1234", result)

        
        const response = result?.data
        if (!total_baris) {
            baris = result.total_baris
        }
        else {
            baris = total_baris
        }
        dispatch({type: SET_GET_DATA_PRODUK, value: {
            data: data.concat(response),
            currentPage,
            category_id,
            total_baris: baris,
            loading: false
        } });
    } catch (error) {
        console.log("data produk", error)
    }
}
export const shiftActions = () => async dispatch => {
    try {
        const shift = await api.apiPrepare.getShift()
        let data = {}
        const time = shift['data']
        if (time) {        
            if (time.length !== 0) {
                data['min'] = time.min.substring(0, time.min.lastIndexOf(':'))
                data['max'] = time.max.substring(0, time.max.lastIndexOf(':'))
            }
        }
        dispatch({type: SET_SHIFT, value: data });
    } catch (error) {
        console.log("shift", error)
    }
}
export const setKeranjang = (value) => async dispatch => {
    let pesanan = decrypt(JSON.parse(localStorage.getItem('pesanan')))
    let x = 0
    if (pesanan) {        
        for(const i of pesanan){
            x += Number(i.item)
        }
    }

    dispatch({type: SET_KERANJANG, value: x})
}
export const SetKategori = (value) => async dispatch => {
    dispatch({type: SET_KATEGORY, value: value});
}