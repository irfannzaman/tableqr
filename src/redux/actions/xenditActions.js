import api from "../../request"
import { SET_COA } from "../constants/xenditConstants"

export const get_CoaXendit = () =>  async dispatch => {
    try {
        const response = await api.apiXendit.coa()
        if (response?.data.length !== 0) {
            dispatch({type: SET_COA, value: response?.data[0]});
        }
    } catch (error) {
        console.log("error", error)
    }
}