import { SET_COA } from "../constants/xenditConstants"

let initialState = {
    coa: ''
}


export const xenditReducer = (state = initialState , action) => {
    if (action.type ===  SET_COA ) {
        return {
            ...state,
            coa: action.value
        }
    }

    return state
}