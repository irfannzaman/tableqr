import { SET_GET_DATA_PRODUK, SET_SHIFT, SET_KATEGORY, SET_KERANJANG, SET_LIST_KATEGORI, SET_ORDER } from "../constants/prepareConstants"

let initialState = {   
    data: {
        loading: true,
        data:[]
    },
    kategory: [],
    shift: {},
    keranjang: 0,
    listKategori: [],
    order: {
        data: [],
        dataOrder: [],
        loading: true
    }
};


export const prepareReducer = (state = initialState , action) => {
    if (action.type ===  SET_GET_DATA_PRODUK ) {
        return {
            ...state,
            data: action.value
        }
    }
    if (action.type ===  SET_KATEGORY ) {
        return {
            ...state,
            kategory: action.value
        }
    }
    if (action.type ===  SET_SHIFT ) {
        return {
            ...state,
            shift: action.value
        }
    }
    if (action.type ===  SET_KERANJANG ) {
        return {
            ...state,
            keranjang: action.value
        }
    }
    if (action.type ===  SET_LIST_KATEGORI ) {
        return {
            ...state,
            listKategori: action.value
        }
    }
    if (action.type ===  SET_ORDER ) {
        return {
            ...state,
            order: action.value
        }
    }
    return state
}