import { createStore, applyMiddleware, combineReducers, compose } from "redux"
import thunk from "redux-thunk"



import { prepareReducer } from "./reducer/prepareReducers"
import { xenditReducer } from "./reducer/xenditReducers"


const initialState = {};



const appReducer = combineReducers({
    dataPrepare: prepareReducer,
    dataXendit: xenditReducer
})


const rootReducer = (state, action) => {
    if (action.type === 'RESET') {
      state = undefined;
    }
    return appReducer(state, action);
}


const composerEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    initialState,
    composerEnhancer(applyMiddleware(thunk)),
  );

export default store;