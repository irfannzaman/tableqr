import { decrypt } from "./enkripsi"


export function refreshCharges() {
    let local_charges = JSON.parse(localStorage.getItem('local_charges'))
    let dataBranch = JSON.parse(localStorage.getItem('dataBranch'))
    var chargeNotes = [];
    local_charges?.forEach(baris=>{
        if (baris.branch_id === dataBranch.branchID) {  
            if (baris.dine_in_only == 1){
                chargeNotes.push({
                    id : baris.id,
                    percent_value : baris.percent_value,
                    min_value : baris.min_value,
                    max_value : baris.max_value,
                    value : 0,
                    coa : baris.coa,
                    pb1 : baris.pb1
                });
            }
            else {
                chargeNotes.push({
                    id : baris.id,
                    percent_value : baris.percent_value,
                    min_value : baris.min_value,
                    max_value : baris.max_value,
                    value : 0,
                    coa : baris.coa,
                    pb1 : baris.pb1
                });
            }
        }
    })
    return chargeNotes
}


export function onHitung(pesanan) {
    const chargeNotes = refreshCharges()    
    let data = {}
    let total = 0
    data.charges_value_non_pb1 = 0;
    data.charges_value_pb1 = 0;

    for(var i = 0; i < pesanan?.length; i++){
        let item = pesanan[i]
        let subTotal = (item.qty * item.price)
        total += subTotal
        data.sub_total = total
    }


    chargeNotes.forEach(d=>{
        if (d.pb1 == 0) {
            if ((data.sub_total > d.max_value) && (d.max_value > 0) ){
                d.value = d.percent_value / 100 * d.max_value;
                data.charges_value_non_pb1 += d.value; 
                
            }
            else {
                d.value = (d.percent_value / 100 * data.sub_total);
                data.charges_value_non_pb1 += d.value; 
            }
        }
        if (d.pb1 == 1) {
            if ((data.sub_total > d.max_value) && ((d.max_value || 0) > 0) ){
                d.value = d.max_value || 0;
                data.charges_value_pb1 += d.value; 
            }
            else {
                d.value = (d.percent_value / 100 * (data.sub_total + data.charges_value_non_pb1));
                data.charges_value_pb1 += d.value; 
            }
        }
    })

    data['grand_total'] = (data['sub_total'] + data.charges_value_non_pb1)
    data['grand_total_net'] = (data['grand_total'] + data.charges_value_pb1)
    data['charges_value'] = (data['charges_value_non_pb1'] + data.charges_value_pb1)
    return {
        chargeNotes: chargeNotes,
        data: data
    }
}