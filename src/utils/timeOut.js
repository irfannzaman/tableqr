export function startTimer(duration) {
    var timer = duration, minutes, seconds, result;
    let myVar = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        let date =  minutes + ":" + seconds;
        if (date == '00:00') {
            clearInterval(myVar);
        }
        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}