export  const dataPay = [
    {
        label: 'Cash',
        data: [    
            {
                name: 'Cash',
                id: 'cash'
            }
        ]
    },
    {
        label: 'E-Wallet',
        data: [    
            {
                name: 'QRIS',
                id: 'qris'
            },
            {
                name: 'Shopee Pay',
                id: 'ID_SHOPEEPAY'
            },
            {
                name: 'OVO',
                id: 'ID_OVO'
            }
        ]
    },

]