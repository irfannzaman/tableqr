import DMSansBold from '../fonts/DMSans-Bold.ttf'
import DMSansMedium from '../fonts/DMSans-Medium.ttf'
import DMSansReguler from '../fonts/DMSans-Regular.ttf'

export {
    DMSansBold,
    DMSansMedium,
    DMSansReguler
}