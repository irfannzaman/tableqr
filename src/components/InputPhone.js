import React from 'react'



export default function InputPhone({ value, label, style, backgroundColor = '#fff', placeholder, onChange, type, disabled, onBlur }) {
    function onChangeInput(val) {
        const input = val.target.value
        if (isNaN(input)) {
            onChange('')
        }
        else {
            if ((value?.length < 1 && input == 0) || (value?.length < 2 && input == '62')) {
                onChange('')
            }
            else {
                onChange(input)
            }
        }
    }
    return (
        <div style={style}>
            {
                label &&
                <div className="fonts500" style={{ paddingBottom: 5, fontSize: 15}}>{label}</div>
            }
            <div style={{ backgroundColor: backgroundColor , display: 'flex', justifyContent: 'center', alignItems: 'center', border: '1px solid #E2E4E8',}}>
                <span className="fonts500" style={{ padding: '0 0 0 10px',  fontSize: 14}}>+62</span>
                <input onBlur={onBlur} disabled={disabled} value={value} type={type} onChange={onChangeInput} required placeholder="8123XXXX" className="fonts400" style={{ backgroundColor: backgroundColor, padding: '0px 12px', height: 35, borderRadius: 5, border: 'none', display: 'inline-block', width: '-webkit-fill-available',  fontSize: 14, width: '100%'}}/>
            </div>
        </div>
    )
}