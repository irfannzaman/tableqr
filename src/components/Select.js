import React from "react";
import "./select.css"
import Gap from "./Gap";

export default function Select({ label, data = [], placholder }) {
    return (

        <div style={{ width: '100%', position: 'relative' }}>
            {
                label &&
                <span className="label_selected">{label}</span>
            }
            {
                label &&
                <Gap height={2}/>
            }
            <select className="selected">
                {
                    placholder &&
                    <option>{placholder}</option>
                }
                {
                    data?.map((items, index) => (
                        <option value={items.value} key={index}>{items.label}</option>
                    ))
                }
            </select>
        </div>
    )
}

