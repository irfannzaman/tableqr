import React from 'react'
import './Navbar.css'
import CopyRight from '../Assets/CopyRight'
import { AiOutlineRight, AiOutlineClose } from "react-icons/ai";
import { BiMenu } from "react-icons/bi";
import { AiOutlineLeft, AiOutlineExclamationCircle } from "react-icons/ai";
import { Link, useHistory } from 'react-router-dom'
import {FaSignOutAlt, FaUser} from "react-icons/fa"
import { getAuth, signOut } from "firebase/auth";
import { AiOutlineCloseCircle } from "react-icons/ai"
import { url, router } from "../Constants"
import Button from "../components/Button"
import Gap from "../components/Gap"
import { IoCartOutline, IoCartSharp } from "react-icons/io5"
import { useSelector } from "react-redux"

export default function Navbar({sidebar, setSidebar, type, label, style = {}, setStyle, urlBack = "back", paramsUrlBack, buttonOrder, cekLokasi, setCekLokasi }) {
    const [loading, setLoading] = React.useState(false)
    let history = useHistory()
    const state = useSelector(state => state?.dataPrepare?.keranjang)   

    function logOut(){
        setLoading(true)
        const auth = getAuth();
        signOut(auth).then(() => {
            history.push( router + '/order')
            localStorage.removeItem("login");
            setLoading(false)
            setSidebar(false)
        }).catch((error) => {
            console.log(error)
        });
    }
    return (
        <div>
            <div className="navBar" style={{ backgroundColor: "#fff" }}>
                <div style={{ width: '100%', display: 'flex', justifyContent: 'space-between'}}>
                    {
                        type == "prepare" ? 
                            <div className="fonts700" style={{ color: '#000', marginLeft: 20, fontSize: 16}}>
                                {label}
                            </div>
                        :
                        <div style={{ display: 'flex', alignItems: 'center', width: '100%'}}>
                            {
                                type !== 'back' ? 
                                    <Link to="#" style={{ marginLeft: 10}} >
                                        <BiMenu onClick={() => {
                                            setSidebar(true)
                                            if (style['position']) {
                                                setStyle({})
                                            }
                                        }} size={25} color="#552CB7"/>
                                    </Link>
                                    :
                                    <button onClick={() => {
                                        if (urlBack == 'back') {
                                            history.goBack()
                                        }
                                        else {
                                            history.push(router + urlBack, paramsUrlBack)
                                        }
                                    }} style={{ marginLeft: 10, border: 'none', backgroundColor: 'transparent'}} >
                                        <AiOutlineLeft  size={25} color="#552CB7"/>
                                    </button>
                            }
                            {
                                !localStorage.getItem('login') && (
                                    <span className="fonts500" style={{ marginLeft: 10, fontSize: 15 }}>
                                        {label ? label : 'Guest'}
                                    </span>
                                )
                            }
                            {
                                localStorage.getItem('login') && (
                                    <span className="fonts500" style={{ marginLeft: 10, fontSize: 15 }}>
                                        {
                                            JSON.parse(localStorage.getItem('login')).nama.indexOf(' ') > 0 ? 
                                                <span className="fonts500" style={{ marginRight: 10}}>
                                                    Halo {JSON.parse(localStorage.getItem('login')).nama.substring(0, JSON.parse(localStorage.getItem('login')).nama.indexOf(' '))}
                                                </span>
                                            :
                                                <span className="fonts500" style={{ marginRight: 10}}>
                                                    Halo {JSON.parse(localStorage.getItem('login')).nama}
                                                </span>
                                        }
                                    </span>
                                )
                            }
                        </div>
                    }
                    {
                        !type &&
                        <div onClick={() => {
                            if( state !== 0  ){
                                history.push(`${router}/detail-order`)
                            }
                        }} style={{ padding: '0px 5px', position: 'relative', display: 'flex', marginRight: 10}} >
                            <IoCartSharp size={25} color="#5451D6"/>
                            {
                                  state !== 0 &&
                                    <div style={{ position: 'absolute', height: '100%', width: '100%', display: 'flex', justifyContent: 'end' }}>
                                        <div style={{ height: 17, width: 17, backgroundColor: 'red', borderRadius: 19, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                            <span style={{ color: '#fff', fontSize: 10, fontWeight: 'bold' }}>{ state < 10 ? state : '+9'}</span>
                                        </div>
                                    </div>
                            }
                        </div>
                    }
                </div>
            </div>

            <div onClick={() => setSidebar(false)}  className={sidebar ? 'nav-menu active' : 'nav-menu'} >
                <div style={Object.assign({ backgroundColor: '#fff', boxShadow: '0px 0px 10px -5px black'}, style)}>
                    {
                        localStorage.getItem('login') && !style['position'] &&
                            <div onClick={() => setSidebar(false)} style={{  textAlign: !style['position']? 'start' : 'end', width: 50, display: 'grid', justifyContent: 'center', paddingTop: 20 }}>
                                <AiOutlineCloseCircle size={30} color="#552CB7"/>
                            </div>
                    }
                    <div className="navBar-header">
                        <span className="fonts700" style={{ marginLeft: 10, fontSize: 15, marginTop: 20 }}>
                            {/* {
                                localStorage.getItem('dataBranch').picpath !== '-' &&
                                <img style={{ height: 40}} src={url + localStorage.getItem('dataBranch').picpath} />
                            } */}
                        </span>
                    </div>
                    <div style={{ marginTop: 20, textAlign: 'center'}}>
                        <CopyRight />
                    </div>
                    <div className="nav-menu-items">
                        {
                            !localStorage.getItem('login') &&
                            <div className="navBar-login">
                                <Button onClick={() => history.push(`${router}/login`)}  backgroundColor='#FFF59D' >Masuk</Button>
                                <Gap height={10} />
                                <Button onClick={() => history.push(`${router}/register`)}  backgroundColor='#552cb7' color="#fff">Daftar</Button>
                                <Gap height={10} />
                                {/* <Button onClick={() => history.push(`/${router}lacak-pesanan`)}  backgroundColor='#552cb7' color="#fff">Lacak Pesanan</Button>
                                <Gap height={20} /> */}
                                {/* <Button onClick={() => setSidebar(false)}  backgroundColor='#552cb7' color="#fff">Continue without Sign up</Button> */}
                                <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                                    <Link to={router + "/register"}>Why sign up?</Link>
                                </div>
                            </div>
                        }
                         {
                            localStorage.getItem('login') &&
                            <div className="navBar-login">
                                <Button onClick={() => logOut()} loading={loading} backgroundColor="#FFF59D">Sign Out</Button>
                                <Gap height={10} />
                                <Button onClick={() => history.push(`${router}/edit-profile`)} backgroundColor="#552cb7" color="#fff">Edit Profile</Button>
                                {/* <Gap height={10} />
                                <Button onClick={() => history.push(`/${router}lacak-pesanan`)} backgroundColor="#552cb7" color="#fff">Lacak Pesanan</Button> */}
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}