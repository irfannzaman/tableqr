import React from "react";
import "./Modal.css"

function Modal({children, visible, style, onClick}) {
    return (
        <div onClick={() => {
            if (onClick) {
                onClick()
            }
        }} className="modalGlobal" style={{transform: `scale(${visible? '1' : '0'})`}}>
          <div className="contentGlobal" style={style}>
              {children}
          </div>
        </div>
    )
}

export default Modal