import React from 'react'
import Navbar from '../../components/Navbar'
import { BsFillBookmarkFill } from 'react-icons/bs'
import { FiEdit } from 'react-icons/fi'
import { useHistory } from 'react-router-dom'
import { getFirestore, doc, getDoc, setDoc} from "firebase/firestore";
import Loading from '../../components/Loading'
import { router } from "../../Constants"

export default function AlamatPengiriman({location}) {
    let history = useHistory()
    const [dataAlamat, setDataAlamat] = React.useState([])
    const [loading, setLoading] = React.useState(true)
    React.useEffect( async () => {
        setLoading(true)
        let phone = JSON.parse(localStorage.getItem('login'))
        if (!phone) {  
            const addressUser  = JSON.parse(localStorage.getItem('without'))  
            if (addressUser) {          
                setDataAlamat([{
                     type: addressUser['alamatSebagai'],
                     alamat: `${addressUser.alamatLengkap} ${addressUser.kecamatan}, ${addressUser.kabupaten}, ${addressUser.provinsi} ${addressUser.kodePos}, ${addressUser.negara}`,
                     detailAlamat: `${addressUser.detailAlamat}`,
                     name: addressUser.namaPenemerima,
                     nomerHp:addressUser.nomerHp,
                     lat: addressUser['latitude'],
                     lng: addressUser['longitude']
                 }])   
            }
        }
        else {
            const db = getFirestore();
            const docRef  = doc(db, "address", phone.telp);
            const docSnap = await getDoc(docRef);
            const docData = docSnap.data()
            if (docData) {            
                setDataAlamat(val => {
                    let data = []
                    for(const items of docData['alamat']){
                        data.push({
                            type: items.alamatSebagai,
                            alamat: `${items.alamatLengkap} ${items.kecamatan}, ${items.kabupaten}, ${items.provinsi} ${items.kodePos}, ${items.negara}`,
                            detailAlamat: `${items.detailAlamat}`,
                            name: items.namaPenemerima,
                            nomerHp:items.nomerHp,
                            lat: items['latitude'],
                            lng: items['longitude'],
                            email: items.email
                        })
                    }
                    return [...val, ...data]
                })
            }
        }
        setLoading(false)
    },[])
    return (
        <div>
            <Navbar urlBack="/detail-order" type="back" label="Alamat Pengiriman"/>
            <div style={{ paddingTop: '20%', margin: '0 10px 0 10px'}}>
                <div className="fonts500">Alamat Tersimpan</div>

                <div style={{ backgroundColor: '#fff', margin: '10px 10px 0 10px'}}>
                    {
                        dataAlamat.map((items, index) => {
                            return (
                                <div key={index} onClick={() => {
                                    localStorage.setItem('alamatUsers', JSON.stringify(items))
                                    const xi = JSON.parse(localStorage.getItem('longlat'))
                                    if (!xi) {     
                                        localStorage.setItem('longlat', false)
                                    }
                                    else {
                                        if (items.lat !== xi.lat && items.lng !== xi.lng) {
                                            localStorage.setItem('longlat', false)
                                        }
                                        else {
                                            localStorage.setItem('longlat', true)
                                        }
                                    }
                                    history.push(`${router}/detail-order`)
                                }}  
                                style={{ padding: '10px', display: 'flex', borderBottom: '1px solid #EFEFF2'}}>
                                    <div style={{ display: 'flex', alignItems: 'flex-start'}}>
                                        <BsFillBookmarkFill color="#5451D6" style={{ marginTop: 2}} />
                                    </div>
                                    <div style={{ marginLeft: '10px', width: '100%'}}>
                                        <div className="fonts700" style={{ fontSize: 14}}>{items['type']}</div>
                                        <div className="fonts400" style={{ fontSize: 12, color: '#BFBFBF', marginTop: 5}}>{items['alamat']}</div>
                                        <div className="fonts400" style={{ fontSize: 13, marginTop: 10}}>Detail Alamat</div>
                                        <div className="fonts400" style={{ fontSize: 12, marginTop: 5, color: '#BFBFBF'}}>{items['detailAlamat']}</div>
                                        <div className="fonts400" style={{ fontSize: 13, marginTop: 10}}>Detail Penerima</div>
                                        <div className="fonts400" style={{ fontSize: 12, marginTop: 5, color: '#BFBFBF'}}>{items['name']}</div>
                                        <div className="fonts400" style={{ fontSize: 12, marginTop: 5, color: '#BFBFBF'}}>{items['nomerHp']}</div>
                                        <div className="fonts400" style={{ fontSize: 12, marginTop: 5, color: '#BFBFBF'}}>{items['email']}</div>

                                        {/* <div style={{ marginTop: 10, display: 'flex' }}>
                                            <FiEdit color="#5451D6"/>
                                            <span className="fonts500" style={{ marginLeft: 10, color:"#5451D6", fontSize: 14}}>Edit</span>
                                        </div> */}
                                    </div>
                                </div>
                            )
                        })
                    }
                    {
                        loading &&
                        <Loading/>
                    }
                </div>

                <div style={{ margin: '20px 10px 10px 10px' }}>
                    <button onClick={() => history.push(`${router}/add-alamat-pengiriman`, {
                        type: 'new',
                        addressUsers: {}
                    })} style={{ width: '100%', padding: '10px 0 10px 0', border: '1px solid #5451D6', borderRadius: 5, display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: '#F3F3FF'}}>
                        <span style={{ fontSize: 13, color: '#5451D6', marginLeft: 5}} className="fonts500">
                            'Tambah Alamat Pengiriman' 
                        </span>
                    </button>
                </div>
            </div>
        </div>
    )
}