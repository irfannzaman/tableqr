import React from 'react'
import Button from '../../components/Button'
import Gap from '../../components/Gap'
import Text from '../../components/Text'
import { AiFillCheckCircle, AiOutlineFieldTime, AiOutlineCloseCircle } from "react-icons/ai"
import { useHistory } from "react-router-dom"
import {router} from "../../Constants"


function TransactionProcessed() {
    let history = useHistory()

    
    return (
        <div style={{ position: 'fixed', height: '100%', width: '100%'}}>
            <div onClick={() => history.push(`${router}/order`)} style={{ marginLeft: 10, marginTop: 10}}>
                <AiOutlineCloseCircle size={30} color="#8B78FF"/>
            </div>
            <div style={{ display: 'grid', justifyContent: 'center', alignItems: 'center', height: '80%',}}>
                <div style={{ margin: '0 10px' }}>
                    <div style={{ display: 'grid', justifyContent: 'center', alignItems: 'center' }}>
                        <AiFillCheckCircle size={130} color="#03B134"/>
                    </div>
                    <Gap height={10} />
                    <div style={{ width: '100%', display: 'grid', justifyContent: 'center', alignItems: 'center' }}>
                        <Text className="fonts700" style={{ fontSize: 25, }}>Transaksi sukses</Text>
                    </div>
                    <Gap height={20} />
                    <div style={{ display: 'flex'}}>
                        <span className="fonts700" style={{ color: "#000", textAlign: 'center', fontSize: 18 }}>Pesanan sedang diproses, mohon menunggu</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TransactionProcessed