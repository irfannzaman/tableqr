import React, { useState, useEffect} from 'react'
import Navbar from '../../components/Navbar'
import api from '../../request'
import { Select, Input, Button , Form} from 'antd';
import { useToasts } from 'react-toast-notifications'
import { socket } from "../../utils/socket"


const { Option } = Select;
const { TextArea } = Input;
export function CallWaiters() {
    const [form] = Form.useForm();
    const { addToast } = useToasts()
    const [sidebar, setSidebar] = React.useState(false)
    const [dataNote, setDataNote] = React.useState([])

    async function getData() {
        const result = await api.apiCallWaiters.defaultNote({
            filter: JSON.stringify({
                "search1": "",
                "search_col1": "all",
                "search2": "",
                "search_col2": "",
                "search3": "",
                "search_col3": "",
                "sort_col1": "branch_id",
                "sort_dir1": "desc",
                "sort_col2": "",
                "sort_dir2": "asc",
                "sort_col3": "",
                "sort_dir3": "asc",
                "jumlah_baris": 10,
                "total_baris": 0,
                "aktif": "all",
                "halaman": 1,
                "kolom_tanggal": "none",
                "showUpdated": false
            })
        })
        const sii = result.data.map((items, index) => ({
            label: items.id,
            value: items.id,
        }))
        setDataNote(sii)
    }

    React.useEffect(() => {
        getData()
        prepareSocket()
    },[])


    function prepareSocket() {        
        socket.on("connect", () => {
            console.log('connected!');
        });
        // socket.emit("call-waiters", {
                
        // })
        // socket.on('saved-si', async (arg) => {
        //     const orderDate = JSON.parse(localStorage.getItem('dataBranch'))
        //     if (arg.trans_id == orderDate.detailData) {
        //         onRefresh('socket')
        //     }
        // });
    }

    async function onClickCallWaiters(values) {
        const branch = JSON.parse(localStorage.getItem("dataBranch"))
        const meja = localStorage.getItem("meja")
        values['meja'] = meja
        values['note'] = values['note']?.toString() + "," + values['note_2']
        try {
            const result = await api.apiCallWaiters.save({
                "mode": "new",
                "detailData": JSON.stringify({
                    customer_name: values['name'],
                    id_klien_note: values['note'],
                    table_id: values['meja']
                })
            })
            addToast("Panggil Waiters Sukses", {
                appearance: 'success',
                autoDismiss: true,
            })
            form.resetFields(["name", "note", "note_2"]);
        } catch (error) {
            addToast("Maaf ada kesalahan", {
                appearance: 'error',
                autoDismiss: true,
            })
        }
    }

    return (
        <div>
            <Navbar label="Panggil Waiters" sidebar={sidebar} setSidebar={(val) => setSidebar(val)}/>
            <div style={{ padding: '20% 0 20px 0', margin: '0 20px', }}>
                <Form
                  form={form}
                  initialValues={{
                    remember: true,
                    size: "small"
                  }}
                  onFinish={onClickCallWaiters}
                  labelCol={{ span: 2, offset: 0 }}
                  wrapperCol={{ span: 2}} 
                  layout="vertical"
                >
                    <Form.Item name="meja" label="Meja">
                        <Input disabled={true} placeholder='Meja' defaultValue={localStorage.getItem("meja")} />
                    </Form.Item>
                    <Form.Item
                      label="Nama"
                       name="name"
                       rules={[
                         {
                           required: false,
                           message: 'Silakan masukan nama anda',
                         },
                       ]}
                     >
                       <Input placeholder="nama" />
                    </Form.Item>
                    <Form.Item name="note" label="Note">
                        <Select
                          mode="multiple"
                          placeholder="Please select"
                          style={{ width: '100%' }}
                        >
                          {
                              dataNote.map((items, index) => (
                                  <Option value={items.label} key={index}>{items.label}</Option>
                              ))
                          }
                        </Select>
                    </Form.Item>
                    <Form.Item name="note_2">
                        <TextArea placeholder='Catatan' rows={4} />
                    </Form.Item>
                    <Form.Item>
                      <Button block type="primary" htmlType="submit" className="login-form-button">
                        Panggil Waiters
                      </Button>
                    </Form.Item>
                </Form>
            </div>  
        </div>
    )
}