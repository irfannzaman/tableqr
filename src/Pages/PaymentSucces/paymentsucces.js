import React from "react";
import Gap from "../../components/Gap";
import "./paymentsucces.scss"
import FETCH_API from "../../config/fetch_api"
import formatterMoney from "../../utils/formatMoney";
import LoadingFull from "../../components/LoadingFull";
import { BiX } from "react-icons/bi";
import { useHistory } from "react-router-dom"
import { router, baseUrl } from "../../Constants"
import Modal from '../../components/Modal';
import InputPhone from "../../components/InputPhone"
import Button from "../../components/Button"
import Input from "../../components/Input";
import { send_email, proses_data, onSave } from "../../config/config"
import { dataPay } from "../../utils/pay"
import { useToasts } from 'react-toast-notifications'
import { onHitung } from "../../utils/onHitung"
import { useSelector } from "react-redux"
import api from "../../request";
import { encrypt, decrypt } from "../../utils/enkripsi"


function PaymentSuccess({ }) {
    const { addToast } = useToasts()
    let history = useHistory()
    const [data, setData] = React.useState({})
    const orderDate = JSON.parse(localStorage.getItem('dataBranch'))

    React.useEffect( async() => {
        const result = await api.apiPrepare.getOrder({
            "id": orderDate.detailData
        })
        const x = onHitung(result.data)
        let data = {}
        data['tanggal'] = result.data[0]['trans_date']
        data['total'] = x.data['grand_total_net']
        setData(data)
        window.addEventListener('popstate', (event) => {
            history.push( router + '/bill' )
        });
    },[])


    return (
        <div>
            {
                data.total ?
                <div className="sea">
                    <div className="circle-wrapper">
                        <div className="bubble"></div>
                        <div className="submarine-wrapper">
                            <div className="submarine-body">
                                <div className="checkmark"></div>
                            </div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                        <span className="fonts700" style={{ fontSize: 23 }}>Payment Success</span>
                        <Gap height={10}/>
                        <span className="fonts500" style={{ fontSize:19 }}>{data.tanggal}</span>
                        <Gap height={50}/>
                        <span className="fonts700" style={{ fontSize: 25 }}>Rp. {formatterMoney(data.total)}</span>
                    </div>
                    <Gap height={30}/>
                    <div style={{ borderBottom: '1px solid #8c8c8c', margin: '0 50px' }}/>
                    <Gap height={30}/>
                    <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                        <span className="fonts700" style={{ fontSize: 15 }}>Makanan yang sudah dibayar tidak bisa dicancel</span>
                        <Gap height={20}/>
                        <span className="fonts700" style={{ fontSize: 23 }}>Thank You</span>
                    </div>
                    <Gap height={50}/>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <div onClick={() =>  history.push(router + '/bill')} className="button_bill">
                            <span style={{ color: '#552CB7' }} className="fonts700">Lihat Bill</span>
                        </div>
                    </div>
                </div>
                :
                <LoadingFull />
            }
        </div>
    )
}

export default PaymentSuccess