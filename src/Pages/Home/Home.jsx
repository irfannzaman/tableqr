import React from 'react'
import './Style.css'
import Head from '../../Assets/Head'
import Responsif from '../../utils/Responsif'
import { Link } from 'react-router-dom'
import {DMSansBold, DMSansMedium, } from '../../fonts/index'
import {router} from "../../Constants"

const button = [
  {
    name : 'Book Table'
  },
  {
    name : 'Order Food Online'
  },
  {
    name : 'GOFOOD'
  },
  {
    name : 'GRABFOOD'
  },
  {
    name : 'Feast At Home'
  },
  {
    name : 'Contact Us On Whatsapp'
  },
]

function Home(params) {
  const { width } = Responsif()
    return(
      <div style={{ textAlign: 'center'}}>
        <Head />
          <div style={{ marginTop: 20 }}>
            <img style={{ height: 40}} src="https://s3-alpha-sig.figma.com/img/9e99/df41/e271ceb7b5d595b7a5638ecd56722041?Expires=1631491200&Signature=LNnSy4vf0pCdX6-Yi2ETq2MGVZwcL~CGT3yaO4ZSulfSaNNr6ECXZD7PTInnXQEXlqkS5nc6TOsuLHjQi3ZY2ObFUTFV8Dt1jITCvfD7vZT-Nto5xFRhQ3cq8MUjOJxoSSegqjpwzcJmrWN5IC4tP5bcvsuXTwEn4MpZQQOPGRacb74kXYbfgfFy-B0qgXIQK7qjBY9jt4njGWWR6cSY2pVHFUVOq1PAw75733whMUoYR354Z75Aoh~LUhi6ZmNyDCleGEJ0L7tKRcfeyy-i22gTZzBLtzwTT~UQ1XHxtDeHykHZ6l5Gle9TyYdgCPc~yBye-4ce2QtLLc6PAZ-WAg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" />
          </div>
          <div className="fonts700" style={{ marginTop: 10, fontSize: 17 }}>
            UNION JAKARTA
          </div>
          <div style={{ marginLeft: 20, marginRight: 20, marginTop: 20 }}>
            {
              button.map((Item, index) => {
                return (
                  <Link to={router +"/pickresto"}>
                    <button 
                      className="fonts500"
                      style={{ 
                        width: '100%', 
                        height: 50,
                        backgroundColor : "#F3F3FF", 
                        color: '#552CB7', 
                        borderWidth: 0,
                        marginTop: 10,
                        fontSize: 16
                    }}>{Item['name']}</button>
                  </Link>
                )
              })
            }
          </div>
          
      </div>
    )
}

export default Home