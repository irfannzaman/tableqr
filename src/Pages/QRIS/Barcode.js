import React, { useState, useEffect } from "react";
import Navbar from '../../components/Navbar'
import QRCode from 'qrcode';
import FETCH_API from "../../config/fetch_api";
import { useHistory } from 'react-router-dom'
import Loading from "../../components/Loading";
import Button from "../../components/Button";
import {formatDate} from "../../utils/formatDate";
import { useToasts } from 'react-toast-notifications'
import { send_email, proses_data } from "../../config/config"
import Gap from "../../components/Gap";
import { router } from "../../Constants"

function Barcode(params) {
    const { state } = params.history.location
    const { addToast } = useToasts()
    console.log("data payment", state)
    let history = useHistory()
    const [imageUrl, setImageUrl] = useState('');
    const [checkPembayaran, setCheckPembayaran] = React.useState(false)
    const [loading, setLoading] = React.useState(false)
    const userServer = JSON.parse(localStorage.getItem("userServer")) 

    async function CekStatus() {
        const dataPeyment = state.params.hasil
        const user = JSON.parse(localStorage.getItem('login'))
        const orderDate = JSON.parse(localStorage.getItem("orderDate"))
        const id = JSON.parse(localStorage.getItem("idpesanan"))
        const checkPayment = await FETCH_API("payment-gateway/checkSuccessXendit", {
            id: state.params['hasil']['external_id'],
            userServer: userServer
        })

        const detailData = []

        for(const items of state.detailTrans){
            detailData.push({
                "namaProduct" : items['name'],
                "qty": items['qty'],
                "price": items['price'],
                "subtotal" : items['total']
            })
        }
        
        if (checkPayment['hasil']['status'] == "ACTIVE") {
            setLoading(false)
            addToast("Pembayaran belum selesai", {
                appearance: 'info',
                autoDismiss: true
            })
        }
        else {
            const result = await proses_data()
            console.log("sukses", result)
            if (result.status == 'OK') {                
                send_email({
                    "subject": 'Pembayaran Uruz',
                    "carabayar": 'QR Code',
                    "idTransaksi" : dataPeyment.id,
                    "subTotal" : dataPeyment.amount,
                    "grandTotal" : dataPeyment.amount,
                    "linkLihatDetail" : "https://google.com",
                    "detailData" : detailData
                    
                })            
                addToast("Pembayaran Berhasil", {
                    appearance: 'success',
                    autoDismiss: true,
                })
                history.push(`${router}/lacak-pesanan`)
            }
        }
    }
    
    
    useEffect(async() => {
        const response = await QRCode.toDataURL(state.params['hasil']['qr_string']);
        setImageUrl(response);
        window.addEventListener('popstate', (event) => {
            history.push(`${router}/order`)
        });
        setTimeout(() => {
            setCheckPembayaran(true)
        }, 10000)
        
    },[])

    return (
        <div>
            <Navbar urlBack='/order' paramsUrlBack = {{
                params: state['params']
            }} type="back" label="Pembayaran QRIS"/>
            <div style={{ paddingTop: '20%'}}>
                <div style={{ margin: '0 20px'}}>
                 <div style={{ fontSize: 17, textAlign: 'center' }} className="fonts600">Buka aplikasi E-Wallet Anda dan Scan QR Code untuk membayar pesanan Anda</div>
                 <Gap height={30}/>
                 <div style={{ color: '#5451D6', fontSize: 17, textAlign: 'center' }} className="fonts700">Pembayaran dapat dilakukan melalui OVO, DANA, LinkAja, ShopeePay dan Sakuku</div>
                </div>
                <div style={{ justifyContent: 'center', display: 'grid', padding: '24px 0'}}>
                    {imageUrl ? (
                      <a href={imageUrl} download>
                          <img style={{ width: 250, height: 250}} src={imageUrl} alt="img"/>
                      </a>) : <Loading />}
                </div>
                <div style={{ margin: '0 10%' }}>
                    {
                        checkPembayaran &&
                        <Button onClick={() => {
                            CekStatus()
                            setLoading(true)
                        }} loading={loading} color="#fff" backgroundColor="#5451D6">
                            Cek Pembayaran
                        </Button>
                    }
                </div>
            </div>
        </div>
    )
}


export default Barcode