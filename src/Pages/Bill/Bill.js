import React from "react";
import Gap from "../../components/Gap";
import Text from "../../components/Text";
import FETCH_API from "../../config/fetch_api";
import formatterMoney from "../../utils/formatMoney";
import LoadingFull from '../../components/LoadingFull';
import CopyRight from '../../Assets/CopyRight'
import { socket } from "../../utils/socket";
import { useHistory } from 'react-router-dom'
import { router, baseUrl } from "../../Constants"
import { onHitung } from "../../utils/onHitung"
import { BiArrowBack } from "react-icons/bi"
import { useSelector, useDispatch } from "react-redux"
import { setOrder } from "../../redux/actions/prepareActions"
import api from "../../request";
import { decrypt } from "../../utils/enkripsi"

function Bill({ }) {
    const dispatch = useDispatch()
    let history = useHistory()
    const type = localStorage.getItem('type')

    const [dataBill, setDFataBill] = React.useState([])

    

    function prepareSocket() {        
        socket.on("connect", () => {
            console.log('connected!');
        });
    
        socket.on('saved-si', async (arg) => {
            const orderDate = JSON.parse(localStorage.getItem('dataBranch'))
            if (arg.trans_id == orderDate.detailData) {
                onRefresh('socket')
            }
        });
    }


    async function onRefresh(value) {
        let dataDetailData = decrypt(JSON.parse(localStorage.getItem('detailData')))
        let detailTrans = decrypt(JSON.parse(localStorage.getItem('detailTrans')))
        const orderDate = JSON.parse(localStorage.getItem('dataBranch'))
        const result = await api.apiPrepare.getOrder({
            "id": orderDate.detailData
        })



        const x = onHitung(result.data)
        let data = []
        let sub_total = []
        sub_total.push({
            label: 'Subtotal',
            value: x?.data?.sub_total
        })

        for(const s of x.chargeNotes){
            sub_total.push({
                label: s.id + " " + s.percent_value + '%',
                value: s.value
            })
        }

        sub_total.push({
            label:'Total',
            value: x?.data?.grand_total_net
        })


        if (localStorage.getItem('typePayment') == "Cash") {
            if (result.data[0].trans_status !== 0 && value) {
                history.push(`${router}/payment-succes`)
            }
        }
        for(const index in result.data){
            const items = result.data[index]
            if (type == "tableQR") {
                if (items.product_id == "QR") {   
                    let subtotal = 0, total = 0, tax = 0
                    for(const xi of result.data){
                        if (xi.product_id !== "QR") {
                            subtotal += (xi.price * xi.qty)
                            total += (xi.price * xi.qty)
                        }
                    }      
                    data.push({
                        id: items.si_id,
                        table_id: items.table_id,
                        tanggal: items.trans_date,
                        data:  result.data.filter(items => items.product_id !== "QR"),
                        total: sub_total,
                        trans_status: items.trans_status
                   })
                }
            }
            if (type == "takeaway") {
                if (index == '0') {              
                    let subtotal = 0, total = 0, tax = 0
                    for(const xi of result.data){
                        subtotal += (xi.price * xi.qty)
                        total += (xi.price * xi.qty)
                    }      
                    data.push({
                        id: items.si_id,
                        table_id: items.table_id,
                        tanggal: items.trans_date,
                        data:  result.data.filter(items => items.product_id !== "QR"),
                        total: sub_total,
                        trans_status: items.trans_status
                    })
                }
            }
        }
        setDFataBill(data)
    }

    React.useEffect(async() => {
        onRefresh()
        prepareSocket()
    },[])

    let trans_status = ""

    if (dataBill.length !== 0 && dataBill[0]['trans_status'] == 0) {
        trans_status = "Pending"
    }
    if (dataBill.length !== 0 && dataBill[0]['trans_status'] == 1) {
        trans_status = "Sukses"
    }



    return (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <div style={{ width: '100%' }}>
                <div onClick={() =>  {
                    dispatch(setOrder())
                    history.push(`${router}/home/lacak-pesanan`)
                }} style={{ padding: '20px 10px', width: '10%'}}>
                    <BiArrowBack size={20}/>
                </div>
            </div>
            <Gap height={50}/>
            <div style={{ justifyContent: 'center', display: 'flex', padding: '10px 0' }}>
                <Text className="fonts700" style={{ color: '#552CB7', fontSize: 17 }}>Your Bill</Text>
            </div>
            {
                dataBill && dataBill.map((items, index) => {

                    console.log("sukses", items)
                    return (
                        <div key={index} style={{ width: '100%' }}>
                            <div style={{ margin: '0 30px' }}>
                                <Text style={{ fontSize: 15 }} className="fonts500">{items.id}</Text>
                            </div>
                            <Gap height={20}/>
                            <div style={{ border: `0.1px solid #BFBFBF`, backgroundColor: '#fff', borderRadius: 10, margin: '0 20px' }}>
                                <div style={{ margin: '20px 20px'}}>
                                    {
                                        type == "tableQR" &&
                                            <div style={{ display: 'flex', justifyContent: 'space-between'}}>
                                                <span style={{ fontSize: 15 }} className="fonts500">Nomer Meja</span>
                                                <span style={{ fontSize: 15 }} className="fonts500">{items.table_id}</span>
                                            </div>
                                    }
                                    <div style={{ display: 'flex', justifyContent: 'space-between'}}>
                                        <Text style={{ fontSize: 15 }} className="fonts500">Tanggal</Text>
                                        <Text style={{ fontSize: 15 }} className="fonts500">{items.tanggal}</Text>
                                    </div>
                                    <div style={{ display: 'flex', justifyContent: 'space-between'}}>
                                        <Text style={{ fontSize: 15 }} className="fonts500">Status Pembayaran</Text>
                                        <Text style={{ fontSize: 15, color: trans_status ==  "Pending" ? "#F8991F" : '#26D044' }} className="fonts700">{trans_status}</Text>
                                    </div>
                                </div>
                            </div>
                            <Gap height={20}/>
                            <div style={{ border: `0.1px solid #BFBFBF`, backgroundColor: '#fff', borderRadius: 10, margin: '0 20px' }}>
                                <div style={{ margin: '20px 20px'}}>
                                    {
                                        items.data.map((item, index) => {

                                            let is_prepared = ""
                                            // if (item.is_prepared == 0) {
                                            //     is_prepared = "Sedang disiapkan"
                                            // }
                                            if (item.is_prepared == 1) {
                                                is_prepared = "Sudah disiapkan"
                                            }
                                            return (
                                                <div key={index}>
                                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                        <div>
                                                            <div style={{ display: 'flex', justifyItems: 'center'}}>
                                                                <span className="fonts700">{item.qty}x</span>
                                                                <Gap width={10} />
                                                                <span className="fonts500">{item.name}</span>
                                                            </div> 
                                                            {
                                                                item.detail_note &&
                                                                <div className="fonts500" style={{ fontSize: 14, color: '#BFBFBF'}}>{item.detail_note}</div>
                                                            }
                                                            {
                                                                is_prepared !== "" &&
                                                                <Gap height={10}/> 
                                                            }
                                                            {
                                                                is_prepared !== "" &&
                                                                <span className="fonts500" style={{ fontSize: 14, color: item.is_prepared == 0 ? "#F8991F" : "#26D044" }}>{is_prepared}</span>
                                                            }
                                                        </div>
                                                        <div>
                                                            <div className="fonts700">Rp.{formatterMoney(item.price)}</div>
                                                        </div>
                                                    </div>
                                                    <Gap height={10}/>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            <Gap height={20}/>
                            <div style={{ border: `0.1px solid #BFBFBF`, backgroundColor: '#fff', borderRadius: 10, margin: '0 20px' }}>
                                <div style={{ margin: '20px 20px'}}>
                                    {
                                        items.total.map((items, index) => (
                                            <div key={index} style={{ display: 'flex', justifyContent: 'space-between', marginTop: 10 }}>
                                                <div>
                                                    <div className="fonts700">{items.label}</div>
                                                </div>
                                                <div>
                                                    <div className="fonts700">Rp.{formatterMoney(items.value)}</div>
                                                </div>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    )
                })
            }
            { dataBill.length == 0  &&
                <LoadingFull />
            }
            <Gap height={40}/>
            <div style={{ marginTop: 20, textAlign: 'center'}}>
                <CopyRight />
            </div>
            <Gap height={20}/>
        </div>
    )
}

export default Bill