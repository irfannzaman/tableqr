import React, { useState, useEffect} from 'react'
import Navbar from '../../components/Navbar'
import { FaConciergeBell } from 'react-icons/fa'
import formatterMoney from '../../utils/formatMoney'
import MenungguPembayaran from '../../Assets/MenungguPembayaran'
import Line from '../../Assets/Line'
import PembayaranDiTerima from '../../Assets/PembayaranDiTerima'
import PesananDiProses from '../../Assets/PesananDiProses'
import PesananDiKirim from '../../Assets/PesananDiKirim'
import PesananSelesai from '../../Assets/PesananSelesai'
import { BiMap } from 'react-icons/bi'
import { useToasts } from 'react-toast-notifications'
import {router} from "../../Constants"
import { IoFastFoodSharp } from "react-icons/io5"

export default function ListLacakPesanan({ history }) {
    const { addToast } = useToasts()
    const { state } = history.location
    const [total, setTotal] = useState({})


    console.log("sukses", state)

    let alamat_users = localStorage.getItem('alamatUsers')
    if (alamat_users == null) {
        alamat_users = null
    }
    else {
        alamat_users = JSON.parse(alamat_users)
    }


    function onHitung(){
        let total = 0
        let qtyTotal = 0
        for(var i = 0; i < state.data.data.length; i++){
            let item = state.data.data[i]
            let subTotal = (item.price * item.qty)
            total += subTotal
            qtyTotal += item.qty
            item.total = subTotal
        }
        // setDataOrder((old) =>[...old])
        setTotal((prevState) => ({
            ...prevState,
            item  : qtyTotal,
            tax   : (total * 10/100),
            total : total,
            grandTotal : total + (total * 10/100)
        }));
    }

    useEffect(() => {
        if (state) {            
            let result = 0
            for(const x of state?.data?.data){
                result += x['total']
            }
            onHitung()    
        }
        else {
            history.push(router + "/home/lacak-pesanan") 
        }
        window.addEventListener('popstate', (event) => {
            history.push(router + "/home/lacak-pesanan")
        });
    },[])
    return (
        <div>
            <Navbar urlBack="/home/lacak-pesanan" type="back" label="Lacak Pesanan"/>
            <div style={{ backgroundColor: '#F0F0F0' }}>
                <div style={{ padding: '25% 0 20px 0', display: 'flex', margin: '0 10px 0 10px', }}>
                    <FaConciergeBell color="#5451D6" size={25} />
                    <div style={{ marginLeft: 20}}>
                        <div className="fonts700" style={{ fontSize: 15}}>{JSON.parse(localStorage.getItem('dataBranch')).branchName}</div>
                        <div className="fonts400" style={{ fontSize: 12, color: '#7E7E7E'}}>{JSON.parse(localStorage.getItem('dataBranch')).branchAddress}</div>
                    </div>
                </div>
            </div>
            <div style={{ margin: '20px 10px 20px 10px' }}>
                <div style={{ margin: '20px 10px 0 10px', backgroundColor: '#fff', borderRadius: 5 }}>
                    <div style={{ display: 'grid', justifyContent: 'center', padding: '10px 0 10px 0' , margin: '20px 10px 0px 10px'  }}>
                        <div className="fonts500">Status Pesanan</div>
                        <div style={{ marginTop: 30, display: 'grid', justifyContent: 'center'}}>
                            <MenungguPembayaran backgroundColor={state?.data['trans_status'] == 0 || state?.data['trans_status'] == 1  ? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <Line backgroundColor={state?.data['trans_status'] == 0 || state?.data['trans_status'] == 1  ? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <PembayaranDiTerima backgroundColor={state?.data['trans_status'] == 1 ? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <Line backgroundColor={state?.data['trans_status'] == 1 ? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <PesananDiProses backgroundColor={state?.data['trans_status'] == 1 && state?.data['is_prepared'] == 0? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <Line backgroundColor={state.data['trans_status'] == 1 && state.data['is_prepared'] == 1? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                            <div style={{ width: 60, height: 60, borderRadius: 60, border: state?.data?.['trans_status'] == 1 && state?.data?.['is_prepared'] == 1 ? '1px solid #5451D6' : '1px solid #C0C4CD', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                                <IoFastFoodSharp size={30} color={state?.data?.['trans_status'] == 1 && state?.data?.['is_prepared'] == 1 ? "#5451D6" : '#C0C4CD'}/>
                            </div>
                            <div style={{ fontSize: 12, marginTop: 10, color: state?.data?.['trans_status'] == 1 && state?.data?.['is_prepared'] == 1 ? "#5451D6" : '#C0C4CD'}}>Pesanan Sudah Siap</div>
                            {/* <PesananDiKirim backgroundColor={state.data?.['trans_status'] == 1 && state.data?.['is_prepared'] == 1? "#5451D6": "#C0C4CD"}/> */}
                        </div>
                        {/* <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <Line backgroundColor={state.data['trans_status'] == 1 && state.data['is_prepared'] == 0? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <PesananDiKirim backgroundColor={state.data['trans_status'] == 1 && state.data['is_prepared'] == 1? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <Line backgroundColor={state.data['trans_status'] == 1 && state.data['is_prepared'] == 1? "#5451D6": "#C0C4CD"}/>
                        </div>
                        <div style={{ marginTop: 10, display: 'grid', justifyContent: 'center' }}>
                            <PesananSelesai backgroundColor={state.data['trans_status'] == 1 && state.data['is_prepared'] == 2? "#5451D6": "#C0C4CD"}/>
                        </div> */}
                    </div>
                </div>

                <div style={{ margin: '20px 10px 0 10px' }}>
                    <div className="fonts500" >Pesanan</div>
                    <div style={{ margin: '20px 10px 0 10px', backgroundColor: '#fff', borderRadius: 5 }}>
                        {
                           state?.data?.data?.map((items, index) => {
                                return (
                                    <div key={index} style={{ display: 'flex', justifyContent: 'space-between', padding: '10px 0 10px 0' , margin: '0px 10px 0px 10px', borderBottom: '1px solid #BFBFBF' }}>
                                        <div>
                                            <div className="fonts700">{items['qty']}x {items['name']}</div>
                                            <div className="fonts500" style={{ fontSize: 14, color: '#BFBFBF'}}>{items['detail_note']}</div>
                                        </div>
                                        <div>
                                            <div className="fonts700">Rp.{formatterMoney(items['total'])}</div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>

                <div style={{ margin: '20px 10px 20px 10px' }}>
                    <div className="fonts500" >Detail Pembayaran</div>
                    <div style={{ margin: '20px 10px 0 10px', backgroundColor: '#fff', borderRadius: 5 }}>
                        {
                            state?.data?.chagerNote?.map((items, index) => (
                                <div key={index} style={{ display: 'flex', justifyContent: 'space-between', padding: '10px 0 10px 0' , margin: '0px 10px 0px 10px', borderTop: state?.data?.chagerNote?.length == (index + 1) ? '1px solid #BFBFBF' : "0" }}>
                                    <div className={ state?.data?.chagerNote?.length == (index + 1) ? "fonts700" : "fonts400"}>{items?.label}</div>
                                    <div className={ state?.data?.chagerNote?.length == (index + 1) ? "fonts700" : "fonts400"} style={{textAlign:'right'}}>Rp.{formatterMoney(items?.value)}</div>
                                </div>
                            ))
                        }
                        {/* <div style={{ display: 'flex', justifyContent: 'space-between', padding: '10px 0 10px 0' , margin: '0px 10px 0px 10px'  }}>
                            <div className="fonts700">Total Pembayaran</div>
                        </div> */}
                        {/* <button onClick={() => {
                            addToast("Untuk membatalkan pesanan bisa menghubungi kasir", {
                                appearance: 'info',
                                autoDismiss: false,
                            })
                        }} style={{width: '100%', margin: '10px 0 10px 0', height: 40, borderRadius: 5, border: 'none', backgroundColor: '#5451D6'}}>
                            <a style={{ color: '#fff', textDecoration: 'none'}} >Batal Pesanan</a>
                        </button> */}
                    </div>
                </div>
            </div>
        </div>
    )
}