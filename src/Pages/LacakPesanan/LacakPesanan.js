import React, { useState, useEffect} from 'react'
import Navbar from '../../components/Navbar'
import { FaConciergeBell } from 'react-icons/fa'
import formatterMoney from '../../utils/formatMoney'
import { useHistory } from 'react-router-dom'
import {formatDate} from '../../utils/formatDate'
import Gap from '../../components/Gap'
import Button from '../../components/Button'
import Loading from '../../components/Loading'
import { router } from "../../Constants"
import { useSelector } from "react-redux"

export default function LacakPesanan() {
    const state = useSelector(state => state?.dataPrepare)
    const order = state?.order
    let history = useHistory();
    const [loadingButton, setLoadingButton] = useState(false)
    const [sidebar, setSidebar] = React.useState(false)

    async function onLacak(items) {
        if (items['status'] == "Menunggu Pembayaran") {
            history.push(router + '/metode-pembayaran/:id', {
                data : items['data'],
                id: items['id'],
            })
        }
        else {
            history.push(router + '/list-lacak-pesanan', {
                data: items
            })
        }
        setLoadingButton(false)
        // let data = []
        // for(const item of dataPesanan){
        //     item['loading'] = false
        //     data.push(item)
        // }
        // setDataPesanan(data)
    }
    
    useEffect(() => {
        window.addEventListener('popstate', (event) => {
            history.push(router + "/home")
        });
    },[])


    return (
        <div>
            <Navbar label="Lacak Pesanan" sidebar={sidebar} setSidebar={(val) => setSidebar(val)}/>
            <div style={{ backgroundColor: '#F0F0F0' }}>
                <div style={{ padding: '25% 0 20px 0', display: 'flex', margin: '0 10px 0 10px', }}>
                    <FaConciergeBell color="#5451D6" size={25} />
                    <div style={{ marginLeft: 20}}>
                        <div className="fonts700" style={{ fontSize: 15}}>{JSON.parse(localStorage.getItem('dataBranch')).branchName}</div>
                        <div className="fonts400" style={{ fontSize: 12, color: '#7E7E7E'}}>{JSON.parse(localStorage.getItem('dataBranch')).branchAddress}</div>
                    </div>
                </div>
            </div>
            <div style={{ margin: '20px 10px 20px 10px' }}>
                {
                    order?.data?.length !== 0 ?
                    <div>
                        {
                            order?.data?.map((items, index) => {
                                if (items && items['trans_status'] == 0) {
                                    items['status'] = "Menunggu Pembayaran"
                                }
                                if (items && items['trans_status'] == 1) {
                                    items['status'] = "Pembayaran Diterima"
                                }
                                if (items && items['trans_status'] == 1 && items['is_prepared'] == 0) {
                                    items['status'] = "Pesanan Diproses"
                                }
                                if (items && items['trans_status'] == 1 && items['is_prepared'] == 1) {
                                    items['status'] = "Pesanan Disiapkan"
                                }
                                return (
                                    <div key={index} style={{ margin: '30px 10px 0 10px', backgroundColor: '#fff', borderRadius: 5 }}>
                                        {/* <div className="fonts400">{ items['id']}</div> */}
                                        <div style={{ display: 'flex', justifyContent: 'space-between', padding: '10px 0 10px 0' , margin: '0px 10px 0px 10px', borderBottom: '1px solid #BFBFBF' }}>
                                            <div>
                                                <div className="fonts400">Tanggal</div>
                                                <Gap height={5} />
                                                <div className="fonts400">Status Pesanan</div>
                                                {
                                                    items['status'] !== "Menunggu Pembayaran" && (
                                                        <Gap height={20} />
                                                    )
                                                }
                                                {
                                                     items['status'] !== "Menunggu Pembayaran" && (
                                                         <div onClick={() => history.push(router + "/bill")} className="fonts700" style={{ color: 'green' }}>Lihat Bill</div>
                                                     )
                                                }
                                            </div>
                                            <div style={{ textAlign: 'end'}}>
                                                <div className="fonts400">{formatDate(items['date'])}</div>
                                                <Gap height={5} />
                                                <div className="fonts400" style={{ color: '#FFB932'}}>{items['status']}</div>
                                            </div>
                                        </div>
                                        {
                                            items.data.map((data, index) => {
                                                return (
                                                    <div key={index} key={index} style={{ display: 'flex', justifyContent: 'space-between', padding: '10px 0 10px 0' , margin: '0px 10px 0px 10px'  }}>
                                                        <div>
                                                            <div className="fonts500">{data['qty']} x   {data['name']}</div>
                                                            <div className="fonts400" style={{ color: '#BFBFBF'}}>{data['detail_note']}</div>
                                                        </div>
                                                        <div className="fonts500">Rp.{formatterMoney(data['price'] * data['qty'])}</div>
                                                    </div>
                                                )
                                            })
                                        }
                                        <div style={{ borderBottom: '1px solid #BFBFBF', margin: '10px 0' }}/>
                                        {
                                            items?.chagerNote?.map((x, index) => (
                                                <div key={index} 
                                                    style={{ 
                                                        display: 'flex', 
                                                        justifyContent: 'space-between',  
                                                        padding: '5px 0', 
                                                        margin: x.label == "Total" ? "10px 10px" :  '0px 10px', 
                                                        borderTop: x.label == "Total" ? '1px solid #BFBFBF' : "0",
                                                        
                                                    }}>
                                                    <span className={x.label == "Total" ? "fonts700" : "fonts500"}>{x.label}</span>
                                                    <span className={x.label == "Total" ? "fonts700" : "fonts500"}>Rp.{formatterMoney(x.value)}</span>
                                                </div>
                                            ))
                                        }
                                        <div style={{ padding: '10px 0 10px 0' , margin: '40px 10px 0px 10px' }}>
                                            {/* <div onClick={() => onLacak(items)} className="fonts700" style={{ marginTop: 10, color: '#5451D6', width: '100%'}}>Lacak Pesanan</div> */}
                                            <div style={{ width: '50%'}}>
                                                <Button 
                                                onClick={() => {
                                                    setLoadingButton(true)
                                                    // let data = []
                                                    // for(const item of dataPesanan){
                                                    //     if (items['id'] == item['id']) {
                                                    //         item.loading = true
                                                    //     }
                                                    //     else {
                                                    //         item.loading = false
                                                    //     }
                                                    //     data.push(item)
                                                    // }

                                                    // setDataPesanan(data)
                                                    onLacak(items)
                                                }} 
                                                    loading={loadingButton} backgroundColor="#5451D6" color="#fff">
                                                    {
                                                        items['status'] == "Menunggu Pembayaran" ? (
                                                            'Bayar Sekarang'
                                                        )
                                                        : (
                                                            'Lacak Pesanan'
                                                        )
                                                    }
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                    : order.loading ? <Loading /> : <div style={{ margin: '20px 10px 0 10px', backgroundColor: '#fff', borderRadius: 5, height: 200, display: 'grid', justifyContent: 'center', alignItems: 'center'}}>
                        <span className="fonts700">Tidak ada data</span>
                    </div>
                }
                
            </div>
        </div>
    )
}