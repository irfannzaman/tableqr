import React from 'react'
import Navbar from '../../components/Navbar'
import { AiOutlineRight, AiOutlineClose } from "react-icons/ai";
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router-dom'
import FETCH_API from '../../config/fetch_api';
import { proses_data, onSave, send_email } from "../../config/config"
import uuid from 'react-uuid'
import { useToasts } from 'react-toast-notifications'
import LoadingFull from '../../components/LoadingFull';
import { dataPay } from "../../utils/pay"
import { encrypt, decrypt } from "../../utils/enkripsi"
import { router, baseUrl } from "../../Constants"
import { formatDate, formatMinutes} from "../../utils/formatDate"
import Gap from '../../components/Gap';
import Modal from '../../components/Modal';
import InputPhone from '../../components/InputPhone'
import Button from '../../components/Button';
import formatPhone from '../../utils/FormatPhone';
import Input from "../../components/Input";
import { useSelector } from "react-redux"
import { onHitung } from "../../utils/onHitung"
import api from '../../request';




export default function MetodePembayaran({ location }) {
    const state = useSelector(state => state?.dataPrepare?.order)
    const xendit = useSelector(state => state?.dataXendit)
    const order = state?.data?.[0]?.data


    const { addToast } = useToasts()
    let history = useHistory()
    const branch = JSON.parse(localStorage.getItem('dataBranch'))
    const userServer = JSON.parse(localStorage.getItem("userServer")) 
    const [loading, setLoading] = React.useState(false)
    const [sidebar, setSidebar] = React.useState(false)
    const [nomerOvo, setNomerOvo] = React.useState('')
    const [dataItems, setDataItems] = React.useState({})
    const [sidebarEmail, setSidebarEmail] = React.useState(false)
    const [detailUsers, setDetailUsers] = React.useState({
        nama: '',
        email: ''
    })
    const [resultPayment, setResultPayment] = React.useState({})

    React.useEffect(async () => {
        window.addEventListener('popstate', (event) => {
            history.push( router + '/home/lacak-pesanan' )
        });
    },[xendit])

    async function onPembayaran(item) {
        const orderDate = JSON.parse(localStorage.getItem('orderDate'))
        const s = onHitung(order)
        localStorage.setItem("chargesNote", JSON.stringify(encrypt(s.chargeNotes)))
        setLoading(true)
        let dataDetailData = decrypt(JSON.parse(localStorage.getItem('detailData')))
        dataDetailData["grand_total_nett"] =  s.data.grand_total_net
        dataDetailData["total"] =  s.data.sub_total
        dataDetailData["payment_cash_coa"] =  "1.01.01.01.01"
        dataDetailData["payment_cash_coa_name"] =  ""
        dataDetailData["payment_cash_value"] =  0
        dataDetailData["sub_total"] =  s.data.sub_total
        dataDetailData["grand_total"] =  s.data.grand_total
        dataDetailData["charges_value"] =  s.data.charges_value
        dataDetailData["kembali"] =  0
        dataDetailData['chargesNote'] = JSON.stringify(s.chargeNotes)
        dataDetailData['payment_others_edc'] = xendit?.coa?.id
        dataDetailData['payment_others_coa'] = xendit?.coa?.coa
        dataDetailData['payment_others_name'] = item['id']
        dataDetailData['payment_others_value'] = s.data.grand_total_net
        dataDetailData['payment_others_note'] = item.name
        dataDetailData['payment_type'] = 3
        
        if (item['name'] == 'QRIS') {
            localStorage.setItem("detailTrans", JSON.stringify(encrypt([])))
            const result = await onSave("proses")
            const dataQR = await api.apiXendit.getQRCodeXendit({
                "id" : branch.detailData,
                "total" : s.data.grand_total_net,
                "trans_code": "si",
                "branch_id": branch.branchID,
            })
            setLoading(false)
            if (dataQR['status'] == 'OK') {                
                localStorage.removeItem('pesanan')
                history.push(`${router}/metode-pembayaran-qris/:id`, {
                    params: dataQR,
                    detailTrans: location.state.data
                })        
            }
        }
        else {
            localStorage.setItem("detailData", JSON.stringify(encrypt(dataDetailData)))
            const res = await onSave("proses")

            const process = await proses_data()

            if(process.status == "ERROR") {
                addToast(process.message, {
                    appearance: 'error',
                    autoDismiss: true,
                })
                setLoading(false)
                return
            }
            else {
                const params = {
                    "from_origin": "*",
                    "userServer" : userServer,
                    "id"   :branch.detailData,
                    "total"      : s.data.grand_total_net,
                    "tipePembayaran" : item['id']
                }
                if (item['id'] == 'ID_SHOPEEPAY') {
                    params['redirectLink'] = `${baseUrl}tableqr/payment-succes`
                }
                if (item['id'] == 'ID_OVO') {
                    params['phoneNumber'] = orderDate.nomerHp
                }
                const result = await FETCH_API('payment-gateway/getXenditWallet', params)
                if (result.status == 'OK') {    
                    localStorage.setItem("id_wallet", JSON.stringify(result.hasil.id))    
                    localStorage.removeItem('pesanan')
                    setResultPayment({
                        result: result,
                        item: item
                    })
                    setLoading(false)
                    setSidebarEmail(true)
                }
                else {
                    alert(result.message)
                    setLoading(false)
                }
            }

        }
    }



        

        async function onSendEmail() {
            const sii = order.map(x => ({
                "namaProduct" : x.name,
                "qty": x['qty'],
                "price": x['price'],
                "subtotal" : x['price'] * x['qty']
            }))
            const s = onHitung(order)
            let data = {}
            data.tanggal = order[0]['trans_date']

            if (detailUsers.nama  == "") {
                addToast("Nama tidak boleh kosong", {
                    appearance: 'error',
                    autoDismiss: true,
                })
                return
            }
            if (detailUsers.email == "") {
                addToast("Email tidak boleh kosong", {
                    appearance: 'error',
                    autoDismiss: true,
                })
                return
            }
            else {
                if(resultPayment.item['id'] == 'ID_SHOPEEPAY'){
                    window.location.assign(resultPayment.result.hasil.actions.mobile_deeplink_checkout_url)
                }
                if (resultPayment.item['id'] == 'ID_OVO') {
                    history.push(`${router}/payment-succes`)
                }
                setSidebarEmail(false)
                send_email({
                    "subject": 'Pembayaran Uruz',
                    "carabayar": localStorage.getItem("typePayment"),
                    "idTransaksi" : branch.detailData,
                    "subTotal" : s.data.sub_total,
                    "grandTotal" : s.data.grand_total + s.data.charges_value_non_pb1,
                    "linkLihatDetail" : `${baseUrl}tableqr/lacak-pesanan`,
                    "detailData" : sii
                })
            }
        }
    



    
    return (
        <div>
            <Navbar urlBack={'/home/lacak-pesanan'} type="back" label="Pilih Metode Pembayaran"/>
            <Modal visible={sidebar} style={{
                    width: '90%',
                    top: '20%',
                    background: '#fff',
                    borderRadius: 15,
            }}>
                <div style={{}}>
                    <Gap height={20} />
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <div className="fonts700" style={{ fontSize: 19 }}>Masukan Nomer OVO anda</div>
                    </div>
                    <Gap height={20} />
                    <div style={{ margin: 20 }}>
                            <InputPhone 
                            onBlur={() => {
                                const nomer = JSON.parse(localStorage.getItem('orderDate'))
                                nomer.nomerHp = '+62' + nomerOvo
                                localStorage.setItem('orderDate', JSON.stringify(nomer))
                            }} 
                            value={nomerOvo} 
                            onChange={(val) => setNomerOvo(val)} 
                            placeholder="8123456789" label="Nomor Handphone" />
                            <Gap height={50} />
                            <Button onClick={() => {
                                if (nomerOvo.length >= 11 && nomerOvo.length <= 15) {
                                    setSidebar(false)
                                    onPembayaran(dataItems)
                                }
                                else {
                                    addToast("Nomor Handphone tidak valid", {
                                        appearance: 'error',
                                        autoDismiss: true,
                                    })
                                }
                            }} loading={false} color="#fff" backgroundColor="#5451D6" >Bayar</Button>
                    </div>
                </div>
            </Modal>
            <Modal visible={sidebarEmail} style={{
                    width: '90%',
                    top: '20%',
                    background: '#fff',
                    borderRadius: 15,
            }}>
                <div style={{}}>
                    <Gap height={20} />
                    <div style={{ display: 'flex', justifyContent: 'center', textAlign: 'center', margin: '0, 20px' }}>
                        <div className="fonts700" style={{ fontSize: 19 }}>Apakah anda ingin menerima bukti bayar lewat email ?</div>
                    </div>
                    <div style={{ margin: 20 }}>
                        <Input onBlur={() => {
                            localStorage.setItem('orderDate', JSON.stringify(detailUsers))
                            }} value={detailUsers['nama']} 
                            onChange={(val) => setDetailUsers((old) => ({
                                ...old,
                                nama: val.target.value
                            }))} 
                        placeholder="Nama" label="Nama" 
                        />
                        <Gap height={20} />
                        <Input onBlur={() => {
                            localStorage.setItem('orderDate', JSON.stringify(detailUsers))
                            }} value={detailUsers['email']} 
                            onChange={(val) => setDetailUsers((old) => ({
                                ...old,
                                email: val.target.value
                            }))} 
                        placeholder="example@gmail.com" label="Email" 
                        />
                        <Gap height={20} />
                        <div style={{ display: 'flex' }}>
                            <Button onClick={() => {
                                if(resultPayment.item['id'] == 'ID_SHOPEEPAY'){
                                    window.location.assign(resultPayment.result.hasil.actions.mobile_deeplink_checkout_url)
                                }
                                if (resultPayment.item['id'] == 'ID_OVO') {
                                    history.push(`${router}/payment-succes`)
                                }
                            }} loading={loading} color="#fff" backgroundColor="#5451D6" >Tidak</Button>
                            <Gap width={20} />
                            <Button onClick={() => onSendEmail()} loading={loading} color="#fff" backgroundColor="#5451D6" >Kirim</Button>
                        </div>
                    </div>
                </div>
            </Modal>
            <div style={{ paddingTop: '20%'}}>
                {
                    <div>
                        {
                            dataPay.filter(items => (xendit?.coa == "" ? items.label == "Cash" : items)).map((items, index) => {
                                return (
                                    <div key={index} style={{ margin: '10px 20px 0px 20px' }}>
                                        <Gap height={20} />
                                        <div className="fonts700" style={{ color: '#552CB7'}}>
                                            {items.label}
                                        </div>
                                        {
                                            items.data.map((items, index) => {
                                                return (
                                                    <div key={index} onClick={() => {
                                                        localStorage.setItem('typePayment', items.name)
                                                        if (items.name !== "Cash") {
                                                            if (items['id'] == 'ID_OVO') {   
                                                                setDataItems(items)   
                                                                setSidebar(true) 
                                                            }
                                                            else {
                                                                onPembayaran(items)
                                                            }
                                                        }
                                                        else {
                                                            history.push(`${router}/bill`)
                                                        }
                                                    }} style={{display: 'flex', backgroundColor: '#fff', height: 60, margin: '10px 0px 0px 0px', borderRadius: 5, boxShadow: '0px 1px 5px -3px #000', justifyContent: 'center' }}>
                                                        <div style={{ margin: 10, width: '100%',  alignItems: 'center', display: 'flex', justifyContent: 'space-between'}}>
                                                            <div>
                                                                <div className="fonts700">
                                                                    {items['name']}
                                                                </div>
                                                                {/* <div className="fonts700">
                                                                    {items['id']}
                                                                </div> */}
                                                            </div>
                                                            <AiOutlineRight color="#552CB7"/>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                )
                            })
                        }
                    </div>
                }
                { (loading)  &&
                    <LoadingFull />
                }
            </div>
        </div>
    )
}


// function detailTrens(items, orderDate, branch, detailData, date) {
//     return {
//         "product_id": items['product_id'],
//         "warehouse_id": detailData['default_warehouse_id'],
//         "coa_inv":  branch.coa_inv,
//         "coa_sales": branch.coa_sales_inv,
//         "name": items['product_name'],
//         "qty": items['item'],
//         "unit_id": "CUP",
//         "qty_unit": 1,
//         "unit_id_small": "PCS",
//         "curr": "IDR",
//         "rate": 1,
//         "price": items['price'],
//         "detail_disc1": 0,
//         "detail_disc2": 0,
//         "detail_disc_value": 0,
//         "detail_note": items['note'],
//         "total": items['total'],
//         "indent": 0,
//         "picpath1_thumb": items['picpath1_thumb'],
//         "ordered_by": orderDate['nama'] == "" ? "Admin" : orderDate['nama'],
//         "ordered_at": date.date + ' ' + date.time,
//         "accepted_rejected_by": "",
//         "accepted_rejected_note": "Chrome",
//         "accepted_rejected_at": null,
//         "prepared_by": "",
//         "prepared_at": "",
//         "kitchen_type_id": "",
//         "promo_name": items['product_name'],
//         "promo_qty":  items['item'],
//         "promo_price": items['price'],
//         "promo_paket": 0,
//         "dtp_bkp": 1
//     }
// }

