import React from 'react'
import { AiOutlineRight, AiOutlineClose } from "react-icons/ai";
import './Tenant.css'
import { Link, useHistory } from 'react-router-dom'
import haversine  from  'haversine'
import FETCH_API from '../../config/fetch_api'
import Navbar from '../../components/Navbar';
import {userServer} from '../../Constants.js'
import Loading from '../../components/Loading';
import { getFirestore, doc, getDoc, setDoc, collection, getDocs} from "firebase/firestore";
import { useEffect } from 'react';
import { router } from "../../Constants"

export default function Tenant() {
    let history = useHistory()
    const [data, setData] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    function selectedBranch(items){
        let getCurrentLoginData = {}
        getCurrentLoginData.branchID = items.id
        getCurrentLoginData.branchName = items.name
        getCurrentLoginData.branchAddress = items.address
        getCurrentLoginData.branchTel = items.mobile
        getCurrentLoginData.picpath = items.picpath
        localStorage.setItem('dataBranch1', JSON.stringify( Object.assign(getCurrentLoginData, items)))
        localStorage.setItem('userServer', JSON.stringify(items.connection))
        history.push(`${router}/pickresto`, items)
    }
    
    useEffect(async () => {
        setLoading(true)
        const db = getFirestore();
        const querySnapshot = await getDocs(collection(db, "clients"));
        querySnapshot.forEach((doc) => {
            const restoran = {
                namaRestoran : doc.id,
                cabang: doc.data().branches,
                connection: doc.data().connection
            }
            setData(val => [...val, restoran])
            setLoading(false)
        });        
    },[])


    console.log("sukses", data)

    return (
        <div className="container">
            <Navbar type="prepare" label="Client"/>
            <div style={{ paddingTop: '20%'}}>
                <div style={{ margin: '10px 20px 0px 20px' }}>
                    {
                        data.map((items, index) => {
                            return (
                                <div onClick={() => {
                                    selectedBranch(items)
                                }} key={index} style={{display: 'flex', backgroundColor: '#fff', height: 60, margin: '10px 0px 0px 0px', borderRadius: 5, boxShadow: '0px 1px 5px -3px #000', justifyContent: 'center' }}>
                                    <div style={{ margin: 10, width: '100%',  alignItems: 'center', display: 'flex', justifyContent: 'space-between'}}>
                                        <div className="fonts700">
                                            {items['namaRestoran']}
                                        </div>
                                        <div className="button-data">
                                            <AiOutlineRight color="#552CB7"/>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    {
                        loading && <Loading />
                    }
                </div>
            </div>
        </div>
    )
}