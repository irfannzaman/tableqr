import React from 'react'
import Navbar from '../../components/Navbar'
import './Order.css'
import { FiPhone } from "react-icons/fi";
import { Link, useHistory } from 'react-router-dom'
import formatterMoney from '../../utils/formatMoney'
import ModalBayar from './ModalBayar';
import { url, router } from '../../Constants.js'
import Loading from '../../components/Loading';
import { useToasts } from 'react-toast-notifications'
import { decrypt, encrypt } from "../../utils/enkripsi"
import api from '../../request';
import { Kategory as LayoutKategory, TabBottom } from "../../Layout"
import { useDispatch, useSelector } from "react-redux"
import { produkActions, shiftActions, getKategory } from "../../redux/actions/prepareActions"
import Logo from "../../Assets/Logo"
import Gap from '../../components/Gap';


export default function Order() {
    const dispatch = useDispatch()
    const state = useSelector(state => state?.dataPrepare)
    const shift = state?.shift
    const produk = state?.data
    const orderData = state?.order
    let history = useHistory()
    const { addToast } = useToasts()
    const [showBayar, setShowBayar] = React.useState(false)
    const [sidebar, setSidebar] = React.useState(false)
    const [dataProduct, setDataProduct] = React.useState([])
    let login = localStorage.getItem('login')
    const [dataOrder, setDataOrder] = React.useState({})
    const [dataBranch, setDataBranch] = React.useState({})
    const [label_error, setlabel_Error] = React.useState({})
    let pesanan = localStorage.getItem('pesanan')
    pesanan = pesanan == null ? [] : decrypt(JSON.parse(pesanan))
    const [style, setStyle] = React.useState({})
    const [order, setOrder] = React.useState({
        item: 0,
        total: 0
    })

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,   
        function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    const handleScroll = () => {
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        const windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom + 1 >= docHeight) {
            if (produk.total_baris > 0) {                
                dispatch(produkActions({
                    ...produk,
                    currentPage : produk.currentPage + 10,
                    total_baris : produk.total_baris - 10
                }))
            }
        }
    };
    
    React.useEffect(() => {
        if (produk.data) {
            setDataProduct(produk.data)
        }
        document.addEventListener('scroll', handleScroll);
    
        return () => {
            document.removeEventListener('scroll', handleScroll);
        };
    }, [produk]);

    React.useEffect(async () => {
        let checkStatus = false
        const prepareLogin = JSON.parse(sessionStorage.getItem('prepareLogin'))
        const s = getUrlVars()
        const branchQueryString = decrypt(s.params)
        const dataBranch = JSON.parse(localStorage.getItem('dataBranch'))
        const userServer = JSON.parse(localStorage.getItem('userServer'))
        dispatch(shiftActions())
        let users = {}
        if (branchQueryString) {
            localStorage.setItem("type", branchQueryString.type)
            localStorage.setItem("meja", branchQueryString.meja)
            localStorage.setItem("userServer", JSON.stringify(branchQueryString.clientID))  
            users.clientID = branchQueryString.clientID
            users.branch_id = branchQueryString.branchID
            const res = await api.apiPrepare.getPos({
                "branches"   : [branchQueryString.branchID]
            }) 
            let items = res.data[0]
            let getCurrentLoginData = {}
            getCurrentLoginData.branchID = items.id
            if (branchQueryString.detailDataID) {
                getCurrentLoginData.detailData = branchQueryString.detailDataID
            }
            getCurrentLoginData.branchName = items.name
            getCurrentLoginData.branchAddress = items.address
            getCurrentLoginData.branchTel = items.mobile
            getCurrentLoginData.picpath = items.picpath
            localStorage.setItem('dataBranch', JSON.stringify(Object.assign(getCurrentLoginData, items)))
            setDataBranch(Object.assign(getCurrentLoginData, items))          
            const local_charges = await api.apiPrepare.getCharge()
            localStorage.setItem("local_charges", JSON.stringify(local_charges.data))
            if (branchQueryString.type == "tableQR") {
                users.detailData = branchQueryString.detailDataID
            }
            localStorage.setItem("userServer", JSON.stringify(branchQueryString.clientID))      
        }
        else {
            if (dataBranch) {          
                users.clientID = userServer
                users.branch_id = dataBranch.branchID
                setDataBranch(dataBranch)
                if (dataBranch.detailData) {
                    users.detailData = dataBranch.detailData
                }
            }
            else {
                checkStatus = true
            }
        }
        if (!login && !prepareLogin) {     
            if(label_error.value == ""){
                setTimeout(() => {
                    setSidebar(true)
                    setStyle({
                        width: '90%',
                        top: '30%',
                        position: 'absolute',
                        left: '5%'
                    })
                }, 2000)    
            }
        }
        if (checkStatus) {
            setlabel_Error({
                label: 'Mohon Scan QR untuk melakukan transaksi',
                value: 2
            })
            dispatch({
                type: 'SET_GET_DATA_PRODUK',
                value: {
                    data: [],
                    loading: false
                }
            })
        }
        else {
            if (produk?.data?.length == 0) {
                updateOrder()
                dispatch(produkActions())
                dispatch(getKategory())
            }
        }

    },[])

    
    function updateOrder() {
        let pesanan = decrypt(JSON.parse(localStorage.getItem('pesanan')))
        let items = 0
        let total = 0
        if (pesanan) {              
            for(const xi of pesanan){
                items += xi['item']
                total += xi['total']
                setOrder({
                    item: items,
                    total: total
                })
            }
        }
    }

    function onOrder(items) {
        setShowBayar(true)
        setDataOrder(items)
    }

    function onClickTambahPesanan(val) {
        addToast("Pesanan sudah ditambah", {
            appearance: 'success',
            autoDismiss: true,
            autoDismissTimeout: 1500,
        })
    }

    if (pesanan.length !== 0) {
        for(const item of dataProduct){
            for(const items of pesanan){
                if (item['product_id'] == items['product_id']) {
                    item['item'] = items['item']
                }
            }
        }
    }
    else {
        for(const item of dataProduct){
            item['item'] = null
        }
    }

    function imageExists(url, callback) {
        var img = new Image();
        img.onload = function() { callback(true); };
        img.onerror = function() { callback(false); };
        img.src = url;
    }


    return (
        <div>
            <Navbar label_error={label_error} setStyle={setStyle} style={style} sidebar={sidebar} setSidebar={(val) => {
                if (!val && style.width) {
                    sessionStorage.setItem('prepareLogin', JSON.stringify('1'))
                }
                setSidebar(val)
            }}/>
            <ModalBayar updateOrder={updateOrder} dataOrder={dataOrder} showBayar={showBayar} setShowBayar={setShowBayar} onClickTambahPesanan={() => {
                if ((Number(order['item']) == 0)) {
                    onClickTambahPesanan()
                }
            }}/>
            <div>
                <div style={{ height: 200, paddingTop: '17%'}}>
                    <img style={{ height:  200, width: '100%', zIndex: "-1", position: 'relative'}} src={url + dataBranch.picpath}/>
                </div>
                                            
                <div style={{ margin: '-30px 20px 0 20px', height: 140, backgroundColor: '#fff', borderRadius: 10, boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)' }}>
                    <div style={{ textAlign: 'center', marginLeft: 10, marginRight: 10 }}>
                        <div className="fonts700" style={{ fontSize: 16, paddingTop: 10}}>{dataBranch.branchName}</div>
                        <div className="fonts500" style={{ fontSize: 12, color: '#7E7E7E'}}>{dataBranch.branchAddress}</div>
                        <div className="fonts700" style={{ paddingTop: 10}}> {shift.min} - {shift.max}</div>
                        <div style={{ display: 'inline-flex', alignItems: 'center', paddingTop: 5}}>
                            {/* <BiMap color="#FF922E" size={15} />
                            <span style={{ fontSize: 12, color: '#7E7E7E', paddingRight: 20, paddingLeft: 5}} className="fonts500">19 KM</span> */}
                            <FiPhone color="#FF922E" size={15} />
                            <span style={{ fontSize: 12, color: '#7E7E7E',  paddingLeft: 5}} className="fonts500">{dataBranch.phone1}</span>
                        </div>
                    </div>
                </div>
                <LayoutKategory />
                <div style={{  margin: '20px 20px 0 20px', backgroundColor: '#fff', paddingBottom: Number(order['item']) > 0 ? 50 : 0 }}>
                    {
                       dataProduct?.length !== 0 &&
                        <div>
                            {
                                dataProduct?.map((items, index) => {
                                    var imageUrl = url+ items.picpath1;
                                    imageExists(imageUrl, function(exists) {
                                        if (exists == true) {
                                            items["show_img"] = true
                                        }else{
                                            items["show_img"] = false
                                        }
                                    });
                                    return (
                                        <div onClick={ () => {
                                            if (orderData?.data?.length !== 0) {                    
                                                if (orderData?.data[0]['trans_status'] < 1) {
                                                    onOrder(items)
                                                }
                                                else {
                                                    addToast("ID sudah pernah digunakan, Mohon Scan QR terbaru untuk melakukan transaksi", {
                                                        appearance: 'success',
                                                        autoDismiss: true,
                                                        // autoDismissTimeout: 1500,
                                                    })
                                                }
                                            }
                                            else {
                                                onOrder(items)
                                            }
                                        }} key={index} style={{ display: 'flex', alignItems: 'center', borderRadius: 5, margin: 10, borderBottom: '1px solid #BFBFBF' }}>
                                            <div style={{ position: 'relative'}}>
                                                {
                                                    items['picpath1']?.length > 3 && items.show_img ?
                                                    <img style={{ height: 100, width: 100, borderRadius: 5, marginBottom: 10}} src={(items['base_url'] !== "" ? items['base_url'] : url) + items['picpath1']}/> :
                                                    <div style={{ 
                                                        height: 100, 
                                                        width: 100, 
                                                        borderRadius: 5, 
                                                        marginBottom: 10, 
                                                        display: 'flex', 
                                                        justifyContent: 'center', 
                                                        alignItems: 'center'
                                                    }}>
                                                        <Logo/>
                                                    </div>
                                                }
                                                {
                                                    items['item'] !== null && items['item'] > 0 &&
                                                    <div 
                                                        style={{
                                                            position: 'absolute', 
                                                            bottom: 14, 
                                                            right: 0, 
                                                            width: 20, 
                                                            backgroundColor: '#fff', 
                                                            textAlign: 'center', 
                                                            fontWeight: 'bold', 
                                                            borderTopLeftRadius: 5, 
                                                            borderBottomRightRadius: 5, 
                                                            border: '1px solid #BFBFBF'
                                                        }}>
                                                        {items['item']}
                                                    </div>
                                                }
                                            </div>
                                            
                                            <div style={{ margin: '0 10px 0 10px' }}>
                                                <div className="fonts700">
                                                    {items['product_name']}
                                                </div>
                                                <span className="fonts700" style={{ marginRight: 10}}>
                                                    {'Rp '+formatterMoney(items['usePrice'])}
                                                </span>
                                                <br/>
                                                <span style={{ marginRight: 10, fontSize:'13px'}}>
                                                    {items.description}
                                                </span>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    }
                    {
                        <Gap height={60}/>
                    }
        
                    {
                        produk?.loading && 
                        <Loading />
                    }
                </div>
                {
                    !produk?.loading &&  dataProduct?.length == 0 &&
                    <div style={{  margin: '20px 20px 0 20px', height: 300, paddingBottom: Number(order['item']) > 0 ? 50 : 0, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                       <span className="fonts700" style={{ fontSize: 25, color: '#5451D6', textAlign: 'center'}}>{label_error.label}</span>
                    </div>
                }
            </div>
        </div>
    )
}