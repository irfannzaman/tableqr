import React from "react";
import './ModalLogin.css'
import { AiOutlineCloseCircle } from "react-icons/ai"

function ModalLogin({showModalLogin, setShowModalLogin}) {
    return (
        <div className="modalLogin" style={{transform: `scale(${showModalLogin})`}}>
            <div className="contentLogin">
                <div style={{ textAlign: 'end' }}>
                    <button onClick={() => {
                        setShowModalLogin('0')
                    }} style={{ border: 'none', backgroundColor: 'transparent'}}>
                        <AiOutlineCloseCircle size={30} color="#8B78FF"/>
                    </button>
                </div>
            </div>
        </div>
    )
}

export default ModalLogin