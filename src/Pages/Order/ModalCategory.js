import React from 'react'
import './ModalCategory.css'
import HeadModal from '../../Assets/HeadModal'
import FETCH_API from '../../config/fetch_api'
import { Link } from 'react-router-dom'
import { decrypt, encrypt } from "../../utils/enkripsi"

export default function ModalCategory({ show, onClickCategory, getUrlVars }) {
  const [dataCategory, setDataCategory] = React.useState([])
  React.useEffect( async() => {
    let tenant = JSON.parse(localStorage.getItem('dataBranch1'))
    const s = getUrlVars()
    const branchQueryString = decrypt(s.params)
    let userServer = JSON.parse(localStorage.getItem("userServer"))
    let dataBranch = JSON.parse(localStorage.getItem("dataBranch"))
    let branch_id = ''
    if (!branchQueryString) {
        branch_id = dataBranch ? dataBranch.branchID : ""
        userServer =  userServer
    }
    else {
        userServer =  branchQueryString.clientID
        branch_id = branchQueryString.branchID
    }

    
      try {
          const res = await FETCH_API('products-web/get-products-category', {
              "from_origin": "*",
              "search1": "",
              // "branch_id" : branch_id,
              "branch_id" : "OE_KOTU",
              // "userServer": userServer
              "userServer": "uruz2"
          })

          console.log("sukses123", res)
          const result = res['data'].map(items => ({
            label: items['id'],
            id: items['id'],
            selected: false
          }))

          setDataCategory([
            {
              label: 'All Categories',
              id: 'all',
              selected: true
            }
          ].concat(result))
      } catch (error) {
          console.log(error)
      }
  },[])

    return (
        <div className="modal" style={{transform: `scale(${show})`}}>
          <div className="content">
              <HeadModal />
              <div className="scroll" style={{ textAlign: 'center', marginTop: 12 }}>
                {
                  dataCategory.map((items, index) => {
                    return (
                      <div key={index}>
                        <div 
                        onClick={() => {
                          for(const data of dataCategory){
                            if (items['id'] == data['id']) {
                              data['selected'] = true
                            }
                            else {
                              data['selected'] = false
                            }
                          }
                          setDataCategory(dataCategory)
                          onClickCategory(items)
                        }} 
                        className={items['selected'] ? "fonts700" : "fonts400"} 
                        style={{ color: items['selected'] ? '#552CB7' :'#7E7E7E', fontSize: items['selected'] ? 15 : 12, textDecoration: 'none'}}
                        >{items['label']}</div>
                      </div>
                    )
                  })
                }
              </div>
          </div>
        </div>
    )
}