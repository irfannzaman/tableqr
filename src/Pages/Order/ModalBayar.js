import React, { useEffect } from 'react'
import './ModalBayar.css'
import { AiOutlinePercentage } from "react-icons/ai";
import { BiPlus , BiMinus, BiX} from "react-icons/bi";
import formatterMoney from '../../utils/formatMoney'
import { encrypt, decrypt } from "../../utils/enkripsi"
import { url } from "../../Constants"
import ImageNotFound from "../../Assets/default-img.jpg"
import { useDispatch } from "react-redux"
import { setKeranjang } from "../../redux/actions/prepareActions"
import Gap from "../../components/Gap"
import Logo from "../../Assets/Logo"  

export default function ModalBayar({showBayar, setShowBayar, onClickTambahPesanan, dataOrder }) {
    const dispatch = useDispatch()

    const [quantity, setQuantity] = React.useState(1)
    const [quantity2, setQuantity2] = React.useState(0)
    const [note, setNote] = React.useState('')
    let pesanan = localStorage.getItem('pesanan')
    pesanan = pesanan == null ? [] : decrypt(JSON.parse(pesanan))
    useEffect(() => {
        if (showBayar) {            
            let pesanan1 = decrypt(JSON.parse(localStorage.getItem('pesanan')))
            setNote('')
            if (pesanan1) {                
                if (pesanan1.length !== 0) { 
                    let number = 1        
                    let price =  0 
                    for(const x of pesanan1){
                        if (dataOrder['product_id'] == x['product_id']) {
                            number = x['item']
                            price = x['item']
                            setNote(x['note'])
                        }
                    }
                    setQuantity(number) 
                    setQuantity2(price)             
                }
            }
        }
    },[showBayar])

    function imageExists(url, callback) {
        var img = new Image();
        img.onload = function() { callback(true); };
        img.onerror = function() { callback(false); };
        img.src = url;
    }

    var imageUrl = url+dataOrder['picpath1'];
    imageExists(imageUrl, function(exists) {
        if (exists == true) {
            dataOrder["show_img"] = true
        }else{
            dataOrder["show_img"] = false
        }
    });


    return (
        <div className={showBayar ? 'modal-menu active' : 'modal-menu'}>
            <div onClick={() => setShowBayar(false)} 
                style={{  
                    margin: '10px 0px 0 10px',
                    position: 'fixed',
                    borderRadius: 40,
                    height: 40, 
                    width: 40,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'rgba(0, 0, 0, 0.3)'
                }} >
                <BiX size={30} color="#fff"/>
            </div>
            {
                dataOrder['show_img'] && (
                    <>
                    {
                        dataOrder['picpath1'] !== '-' && dataOrder['picpath1'] ?
                        <img style={{ 
                                // height: '40%', 
                                width: '100%', 
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20, 
                                marginBottom: 10,
                                }} src={url+dataOrder['picpath1']}/>
                        :
                        <img style={{ 
                             // height: '40%', 
                             width: '100%', 
                             borderTopLeftRadius: 20,
                             borderTopRightRadius: 20, 
                             marginBottom: 10,
                             }} src={ImageNotFound}/>
                    }
                    </>
                )   
            }

            {
                !dataOrder['show_img'] && <div style={{
                    width: '100%', 
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20, 
                    marginBottom: 10,
                    height: "40%",
                    justifyContent: 'center',
                    alignItems: 'center',
                    display: 'flex'
                }}>
                    <Logo height='50' width='200'/>
                </div>
            }
            <div style={{ width: '100%' }}>
                <div style={{ margin: 20}}>
                    <div className="fonts700" style={{ fontSize: 16, paddingTop: 5}}>{dataOrder['product_name']}</div>
                    <span className="fonts700" style={{ fontSize: 16, marginRight: 10, paddingTop: 5}}>{"Rp "+formatterMoney(dataOrder['usePrice'])}</span>
                    <div style={{ paddingTop: 5 }}>
                        <span style={{ fontWeight: '700', fontSize: 15 }}>Detail Produk</span>
                        <Gap height={5}/>
                        <div className="fonts400" style={{ fontSize: 14, color: '#7E7E7E'}}>{dataOrder['description'] == "" ? "-" : dataOrder['description']}</div>
                    </div>
                    <div>
                        <textarea value={note} onChange={(val) => setNote(val.target.value)} style={{height: '90px',marginTop:'20px', marginBottom:'20px', border: '1px solid #BFBFBF',  borderRadius: 5 }} placeholder="Order note..." />
                    </div>

                    <div style={{ display: 'flex', paddingTop: 20 }}>
                        <div style={{ display: 'flex', width: '40%', backgroundColor: '#fff', justifyContent: 'space-between', alignItems: 'center' }}>
                            <button onClick={() => {
                                setQuantity(val => {
                                    if (val > 1) {
                                        return  val - 1
                                    }
                                    else {
                                        return val
                                    }
                                })
                            }} style={{ border: 'none', height: 40, width: 40, border: `1px solid ${quantity !== 1 ? '#5451D6' : '#BFBFBF'}`, borderRadius: 10, backgroundColor: '#fff', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                <BiMinus size={20} color={quantity !== 1 ? "#5451D6" : "#BFBFBF"}/>
                            </button>
                            <span className="fonts700" style={{ fontSize: 20}}> {quantity != 0 ? quantity : 1 } </span>
                            <button onClick={() => {
                                setQuantity((val) => {
                                    return val + 1
                                })
                            } } style={{ border: 'none', height: 40, width: 40, border: '1px solid #5451D6', borderRadius: 10, backgroundColor: '#fff', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                <BiPlus size={20} color="#5451D6" />
                            </button>
                        </div>
                        <div style={{ width: '60%', display: 'flex', justifyContent: 'flex-end' }}>
                            <button onClick={() => {
                                let name = []
                                for(const xi of pesanan){
                                    if (xi['product_id'] == dataOrder['product_id']) {
                                        name.push(xi['product_id'])
                                    }
                                }
                                if (name.includes(dataOrder['product_id'])) {
                                    for(const resultOrder of pesanan){
                                        if (resultOrder['product_id'] == dataOrder['product_id']) {
                                            resultOrder['item'] = quantity 
                                            resultOrder['total'] = dataOrder['usePrice'] * quantity
                                        }
                                    }
                                }
                                else {
                                    pesanan.push(Object.assign(dataOrder, {
                                        item: quantity,
                                        total: dataOrder['usePrice'] * quantity,
                                        note: note
                                    }))
                                }
                                const resultPesanan = pesanan.filter(items => items['item'].toString() !== '0')
                                localStorage.setItem('pesanan', JSON.stringify(encrypt(resultPesanan)))
                                setShowBayar(false)
                                dispatch(setKeranjang())
                                if (quantity > quantity2) {
                                    onClickTambahPesanan()
                                }
                            }} style={{ padding: '7px 0 7px 0',  borderRadius: 5, backgroundColor: '#552CB7', width: '80%', marginRight: 10, display: 'flex', justifyContent: 'space-around', border: 'none', alignItems: 'center'}}>
                                <div style={{ color: '#fff'}} className="fonts500">Tambah Pesanan</div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}