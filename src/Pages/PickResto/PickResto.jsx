import React from 'react'
import { AiOutlineRight, AiOutlineClose } from "react-icons/ai";
import './style.css'
import { Link, useHistory } from 'react-router-dom'
import FETCH_API from '../../config/fetch_api'
import Navbar from '../../components/Navbar';
import Loading from '../../components/Loading';
import { router } from "../../Constants"

import { useEffect } from 'react';

export default function PickResto({ location }) {
    // const { items } = params.history.location.state
    let history = useHistory()
    const [data, setData] = React.useState([])
    const [loading, setLoading] = React.useState(true)
    

    function selectedBranch(items){
        let getCurrentLoginData = {}
        getCurrentLoginData.branchID = items.id
        getCurrentLoginData.branchName = items.name
        getCurrentLoginData.branchAddress = items.address
        getCurrentLoginData.branchTel = items.mobile
        getCurrentLoginData.picpath = items.picpath
        localStorage.setItem('dataBranch', JSON.stringify( Object.assign(getCurrentLoginData, items)))
    }

    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };


      
     async function success(pos) { 
        const userServer = JSON.parse(localStorage.getItem("userServer"))
        let branch = []
        var crd = pos.coords;
        for(const dataBranch of location.state.cabang){
            branch.push(dataBranch.branch_id)
        }
        const res = await FETCH_API('branches-web/get-pos', {
            "from_origin": "*",
            "userServer" : userServer,
            "branches"   : branch
        }) 
        if (res) {
            
            
            console.log("sukses1", crd)           
            console.log("sukses2", res)           
            // for(const item of res['data']){
            //     var from = new window.google.maps.LatLng(crd.latitude, crd.longitude);
            //     var dest = new window.google.maps.LatLng(item.latitude, item.longitude);
            //     var service = new window.google.maps.DistanceMatrixService();
            //     const xi = await service.getDistanceMatrix(
            //         {
            //             origins: [from],
            //             destinations: [dest],
            //             travelMode: 'DRIVING'
            //         }, (response, status) => {
            //             if (status == 'OK') {  
            //                 return response
            //             }
            //         });
            //         item['locations'] = xi.rows[0].elements[0]['distance']['text']
            //     }
            setData(res['data'])
        }
        setLoading(false)
    }
    
    function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
        setLoading(false)
      }
    
    useEffect(async () => {
        setLoading(true)
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success, error, [options])
        }
        
    },[])


    return (
        <div className="container">
            <Navbar type="prepare" label="Pickup & Delivery"/>
            <div style={{ paddingTop: '20%'}}>
                <div style={{ margin: '10px 20px 0px 20px' }}>
                    {
                        data.map((items, index) => {
                            return (
                                <div onClick={() => {
                                    selectedBranch(items)
                                    history.push(`${router}/order`)
                                }} key={index} style={{display: 'flex', backgroundColor: '#fff', height: 60, margin: '10px 0px 0px 0px', borderRadius: 5, boxShadow: '0px 1px 5px -3px #000', justifyContent: 'center' }}>
                                    <div style={{ margin: 10, width: '100%',  alignItems: 'center', display: 'flex', justifyContent: 'space-between'}}>
                                        <div>
                                            <div className="fonts700">
                                                {items['name']}
                                            </div>
                                            <div className="fonts500">
                                                {items['locations']}
                                            </div>
                                        </div>
                                        <div className="button-data">
                                            <AiOutlineRight color="#552CB7"/>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    {
                        loading && <Loading />
                    }
                </div>
            </div>
        </div>
    )
}