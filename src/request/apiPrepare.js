import baseRequest  from "../utils/request"


export default class Prepare extends baseRequest {
    constructor() {
        super();
        this.urls = {
            produk: "products-web",
            kategory: 'products-web/get-products-category',
            getPos: 'branches-web/get-pos',
            get: 'trans-s-si-web/get',
            getShift: 'products-web/get-shift',
            coaBranchAll: 'coa-web/get-default-branch-all',
            getCharge: 'products-web/get-charges',
            getOrder: 'trans-s-si-web/get',
        }
    }

    produk = params => this.post(this.urls.produk, params)
    kategory = params => this.post(this.urls.kategory, params)
    getPos = params => this.post(this.urls.getPos, params)
    get = params => this.post(this.urls.get, params)
    getShift = params => this.post(this.urls.getShift, params)
    coaBranchAll = params => this.post(this.urls.coaBranchAll, params)
    getCharge = params => this.post(this.urls.getCharge, params)
    getOrder = params => this.post(this.urls.getOrder, params)
}