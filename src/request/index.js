import Prepare from "./apiPrepare"
import Xendit from "./apiXendit"
import CallWaiters from "./apiCallWaiters"

const api = {
    apiPrepare : new Prepare(),
    apiXendit: new Xendit(),
    apiCallWaiters: new CallWaiters(),
}
export default api