import baseRequest  from "../utils/request"


export default class CallWaiters extends baseRequest {
    constructor() {
        super();
        this.urls = {
            getData: "notes-klien/getAllNoteTrans",
            defaultNote: "notes-klien/defaultNotes",
            save: "notes-klien/saveTransNoToken",
        }
    }

    getData = params => this.post(this.urls.getData, params)
    defaultNote = params => this.post(this.urls.defaultNote, params)
    save = params => this.post(this.urls.save, params)
}