import baseRequest  from "../utils/request"


export default class Xendit extends baseRequest {
    constructor() {
        super();
        this.urls = {
            getQRCodeXendit: "payment-gateway/getQRCodeXendit",
            checkSuccess: "payment-gateway/checkSuccessWalletXendit",
            coa: "products-web/get-coa-xendit"
        }
    }

    getQRCodeXendit = params => this.post(this.urls.getQRCodeXendit, params)
    checkSuccess = params => this.post(this.urls.checkSuccess, params)
    coa = params => this.post(this.urls.coa, params)
}