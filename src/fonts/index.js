import DMSansBold from './DMSans-Bold.ttf'
import DMSansMedium from './DMSans-Medium.ttf'
import DMSansReguler from './DMSans-Regular.ttf'

export {
    DMSansBold,
    DMSansMedium,
    DMSansReguler
}