const init = {
    userId: '',
    distanceMatrixService: ''
}

const authReducer = (state = init, action) => {
    switch(action.type) {
        case "LOGIN_SUCCESS":
            return {
                ...state,
                userId: action.payload
            }
        case "DISTANCE_MATRIX":
            return {
                ...state,
                distanceMatrixService: action.payload
            }
        default:
            return state
    }
}

export default authReducer