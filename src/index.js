import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { combineReducers, createStore } from 'redux'
import './index.css';
import Apps from './View/Apps'
import { ToastProvider, useToasts } from 'react-toast-notifications';
import reportWebVitals from './reportWebVitals';
// import 'antd/dist/antd.css';
import './View/styleGlobal.css'
import authReducer from "./store/store"
import * as serviceWorker from './serviceWorker';
import store from "./redux/store"
import 'antd/dist/antd.css'

const reducers = combineReducers(
  {
      auth: authReducer,
      // booking: bookingReducer
  }
)

const reduxStore = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)


ReactDOM.render(
    <Provider store={store}>
      <ToastProvider>
        <Apps/>
      </ToastProvider>
    </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
// serviceWorker.unregister();
